<?php
/**
 * @file
 * tvu_feedback.features.inc
 */

/**
 * Implements hook_node_info().
 */
function tvu_feedback_node_info() {
  $items = array(
    'feedback' => array(
      'name' => t('Phản biện'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
