<?php
/**
 * @file
 * tvu_web_master_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function tvu_web_master_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-web-master-menu_bi-nghin-cu:admin/projects.
  $menu_links['menu-web-master-menu_bi-nghin-cu:admin/projects'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/projects',
    'router_path' => 'admin/projects',
    'link_title' => 'Bài nghiên cứu',
    'options' => array(
      'identifier' => 'menu-web-master-menu_bi-nghin-cu:admin/projects',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-web-master-menu_biu-mu:admin/form.
  $menu_links['menu-web-master-menu_biu-mu:admin/form'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/form',
    'router_path' => 'admin/form',
    'link_title' => 'Biểu mẫu',
    'options' => array(
      'identifier' => 'menu-web-master-menu_biu-mu:admin/form',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-web-master-menu_cms-page:admin/cms-page.
  $menu_links['menu-web-master-menu_cms-page:admin/cms-page'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/cms-page',
    'router_path' => 'admin/cms-page',
    'link_title' => 'CMS Page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_cms-page:admin/cms-page',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-web-master-menu_danh-sch-tin-tc:admin/articles-manager.
  $menu_links['menu-web-master-menu_danh-sch-tin-tc:admin/articles-manager'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/articles-manager',
    'router_path' => 'admin/articles-manager',
    'link_title' => 'Danh sách tin tức',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_danh-sch-tin-tc:admin/articles-manager',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 1,
    'customized' => 1,
  );
  // Exported menu link: menu-web-master-menu_hnh-nh:admin/images.
  $menu_links['menu-web-master-menu_hnh-nh:admin/images'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/images',
    'router_path' => 'admin/images',
    'link_title' => 'Hình ảnh',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_hnh-nh:admin/images',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: menu-web-master-menu_thm-mi:node/add/article.
  $menu_links['menu-web-master-menu_thm-mi:node/add/article'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'node/add/article',
    'router_path' => 'node/add/article',
    'link_title' => 'Thêm mới',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_thm-mi:node/add/article',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
    'parent_identifier' => 'menu-web-master-menu_danh-sch-tin-tc:admin/articles-manager',
  );
  // Exported menu link: menu-web-master-menu_thm-mi:node/add/gallery.
  $menu_links['menu-web-master-menu_thm-mi:node/add/gallery'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'node/add/gallery',
    'router_path' => 'node/add/gallery',
    'link_title' => 'Thêm mới',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_thm-mi:node/add/gallery',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
    'parent_identifier' => 'menu-web-master-menu_hnh-nh:admin/images',
  );
  // Exported menu link: menu-web-master-menu_thm-mi:node/add/magazine.
  $menu_links['menu-web-master-menu_thm-mi:node/add/magazine'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'node/add/magazine',
    'router_path' => 'node/add/magazine',
    'link_title' => 'Thêm mới',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_thm-mi:node/add/magazine',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
    'parent_identifier' => 'menu-web-master-menu_tp-ch-khoa-hc:admin/magazine',
  );
  // Exported menu link: menu-web-master-menu_thm-mi:node/add/static-page.
  $menu_links['menu-web-master-menu_thm-mi:node/add/static-page'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'node/add/static-page',
    'router_path' => 'node/add/static-page',
    'link_title' => 'Thêm mới',
    'options' => array(
      'attributes' => array(
        'title' => 'Use <em>basic pages</em> for your static content, such as an \'About us\' page.',
      ),
      'identifier' => 'menu-web-master-menu_thm-mi:node/add/static-page',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
    'parent_identifier' => 'menu-web-master-menu_cms-page:admin/cms-page',
  );
  // Exported menu link: menu-web-master-menu_thm-mi:node/add/video.
  $menu_links['menu-web-master-menu_thm-mi:node/add/video'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'node/add/video',
    'router_path' => 'node/add/video',
    'link_title' => 'Thêm mới',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_thm-mi:node/add/video',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
    'parent_identifier' => 'menu-web-master-menu_videos:admin/videos',
  );
  // Exported menu link: menu-web-master-menu_tp-ch-khoa-hc:admin/magazine.
  $menu_links['menu-web-master-menu_tp-ch-khoa-hc:admin/magazine'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/magazine',
    'router_path' => 'admin/magazine',
    'link_title' => 'Tạp chí khoa học',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_tp-ch-khoa-hc:admin/magazine',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 3,
    'customized' => 1,
  );
  // Exported menu link: menu-web-master-menu_videos:admin/videos.
  $menu_links['menu-web-master-menu_videos:admin/videos'] = array(
    'menu_name' => 'menu-web-master-menu',
    'link_path' => 'admin/videos',
    'router_path' => 'admin/videos',
    'link_title' => 'Videos',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-web-master-menu_videos:admin/videos',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 4,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Biểu mẫu');
  t('Bài nghiên cứu');
  t('CMS Page');
  t('Danh sách tin tức');
  t('Hình ảnh');
  t('Thêm mới');
  t('Tạp chí khoa học');
  t('Videos');

  return $menu_links;
}
