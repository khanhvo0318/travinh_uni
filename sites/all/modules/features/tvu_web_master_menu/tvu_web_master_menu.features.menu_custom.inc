<?php
/**
 * @file
 * tvu_web_master_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function tvu_web_master_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-web-master-menu.
  $menus['menu-web-master-menu'] = array(
    'menu_name' => 'menu-web-master-menu',
    'title' => 'Web Master Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Web Master Menu');

  return $menus;
}
