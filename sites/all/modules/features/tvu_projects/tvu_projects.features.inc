<?php
/**
 * @file
 * tvu_projects.features.inc
 */

/**
 * Implements hook_node_info().
 */
function tvu_projects_node_info() {
  $items = array(
    'projects' => array(
      'name' => t('Bài gửi'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Tiêu đề'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
