<?php
/**
 * @file
 * tvu_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function tvu_roles_user_default_roles() {
  $roles = array();

  // Exported role: Ban biên tập.
  $roles['Ban biên tập'] = array(
    'name' => 'Ban biên tập',
    'weight' => 4,
  );

  // Exported role: Ban biên tập hình thức.
  $roles['Ban biên tập hình thức'] = array(
    'name' => 'Ban biên tập hình thức',
    'weight' => 5,
  );

  // Exported role: Ban biên tập tiếng anh.
  $roles['Ban biên tập tiếng anh'] = array(
    'name' => 'Ban biên tập tiếng anh',
    'weight' => 6,
  );

  // Exported role: Thư ký.
  $roles['Thư ký'] = array(
    'name' => 'Thư ký',
    'weight' => 3,
  );

  return $roles;
}
