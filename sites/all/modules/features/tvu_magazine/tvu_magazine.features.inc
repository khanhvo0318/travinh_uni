<?php
/**
 * @file
 * tvu_magazine.features.inc
 */

/**
 * Implements hook_node_info().
 */
function tvu_magazine_node_info() {
  $items = array(
    'magazine' => array(
      'name' => t('Tạp chí khoa học'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Tiêu đề'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
