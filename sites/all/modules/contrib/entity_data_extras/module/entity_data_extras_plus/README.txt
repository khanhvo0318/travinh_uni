Entity Data Extras Plus

Description
--------------------------
Implements of Entity Data Extras:
- Suport add extras class(es) to Node and Bean.
- Suport chose template to Node and Bean.
- Suport chose page template when view full Node.

Installation
--------------------------
1. Copy the entire module directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module on the Modules page.
3. Config Node content types or Bean content types at
   admin/config/content/entity_data_extras_plus
