<?php


/**
 * Implementation of hook_form_block_admin_configure_alter().
 */
function entity_data_extras_plus_form_block_admin_configure_alter(&$form, &$form_state) {
  // Create fake entity bundle.
  $form['#bundle'] = 'block';
  $form['#entity_type'] = 'block';
  $form['#entity'] = block_load($form['module']['#value'], $form['delta']['#value']);
  if (isset($form['#entity']->data_extras)) {
    $form['#entity']->data_extras = unserialize($form['#entity']->data_extras);
    if (!is_array($form['#entity']->data_extras)) {
      $form['#entity']->data_extras = unserialize($form['#entity']->data_extras);
    }
  }

  entity_data_extras_plus_form_attach($form, $form_state);
  $form['#after_build'][] = 'entity_data_extras_plus_block_form_after_build';
  $form['#submit'][] = 'entity_data_extras_plus_block_form_submit';
}


function entity_data_extras_plus_block_form_after_build($form, &$form_state) {
  $form['data_extras']['#weight'] = $form['settings']['#weight'] + 0.0001;
  return $form;
}

/**
 * Implementation of hook_form_block_add_block_form_alter().
 */
function entity_data_extras_plus_form_block_add_block_form_alter(&$form, &$form_state) {
  entity_data_extras_plus_form_block_admin_configure_alter($form, $form_state);
}

/**
 * Implement entity_data_extras_plus_block_form_submit.
 */
function entity_data_extras_plus_block_form_submit($form, &$form_state) {
  db_update('block')
    ->fields(array('data_extras' => serialize($form_state['values']['data_extras'])))
    ->condition('module', $form_state['values']['module'])
    ->condition('delta', $form_state['values']['delta'])
    ->execute();
}



/**
 * Implements theme_preprocess_block().
 *
 * Extend block's classes with any user defined classes.
 */
function entity_data_extras_plus_preprocess_block(&$variables) {
  // Check if block display by panels.
  if (isset($variables['block']->subtype)) {
    $block_data = block_load($variables['block']->module, $variables['block']->delta);
    if (isset($block_data->data_extras)) {
      $variables['block']->data_extras = $block_data->data_extras;
    }
  }

  if (!empty($variables['block']->data_extras)) {
    $variables['block']->data_extras = unserialize($variables['block']->data_extras);
    if (!is_array($variables['block']->data_extras)) {
      $variables['block']->data_extras = unserialize($variables['block']->data_extras);
    }
    $variables['entity_type'] = 'block';
    $variables['block']->type = 'block';
    entity_data_extras_plus_preprocess_entity($variables);
  }
}
