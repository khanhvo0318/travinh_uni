Entity Data Extras

Description
--------------------------
Add data_extras field to Entity, used as $user->data field.
Current support for Node, Bean.

Installation
--------------------------
1. Copy the entire blade directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module on the Modules page.
