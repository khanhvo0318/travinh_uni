/**
 * Created by ZM on 22/9/2014.
 */
(function ($) {
    $(document).ready(function () {
        if(!$("body").hasClass("page-admin-structure-views")){
            $("#zmmenu-left").mmenu({
                searchfield: true,
                //counters: true,
                slidingSubmenus: false,
                header: {
                    add: true,
                    update: true,
                    title: 'ADMIN MENU <i class="glyphicon glyphicon-circle-arrow-left"></i>'
                }
            }, {
                classNames: {
                    selected: "active-trail"
                }
            });

            $("html").addClass("has-zmmenu");
        }
    });
})(jQuery);