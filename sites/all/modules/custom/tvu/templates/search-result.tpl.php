<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/9/2016
 * Time: 1:42 PM
 */
?>
<div class="section-news section-news-detail page-search-result ">
  <h5 class="title-page">Kết quả tìm kiếm cho từ khóa "<?php print $data['key'] ?>".</h5>
  <?php if(!empty($data['node'])): ?>
  <div class="news-related">
    <div class="news-related-content">
      <?php print render($data['node']); ?>
    </div>
    <div class="pagination">
      <?php print theme('pager');  ?>
    </div>
  </div>
    <?php else : ?>
    <div class="no-result">Không có kết quả tìm kiếm phù hợp.</div>
  <?php endif;?>
</div>
