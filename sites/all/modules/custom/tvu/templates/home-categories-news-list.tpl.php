<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 8:28 PM
 */
$voca = taxonomy_vocabulary_machine_name_load('categories');
$cats = taxonomy_get_tree($voca->vid);
?>
<?php foreach ($cats as $cat) :?>
  <div class="cat-item">
    <h5 class="cat-item-name"><?php print $cat->name; ?></h5>
    <div class="cat-item-content">
      <?php
        $lasted = _tvu_loadFirstNewsByCat($cat->tid);
        print render($lasted);
      ?>
      <div class="cat-item-list">
        <div>
          <?php
            $other = _tvu_loadOtherNewsByCat($cat->tid);
            print render($other);
          ?>
        </div>
        <div class="readmore"><a href="<?php print url('taxonomy/term/'.$cat->tid); ?>">Xem Thêm<i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
      </div>
    </div>
  </div>
<?php endforeach;?>


