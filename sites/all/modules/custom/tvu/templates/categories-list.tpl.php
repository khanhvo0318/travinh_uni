<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/3/2016
 * Time: 10:19 PM
 */
?>
<?php if(!empty($variables['data']['node'])):?>
<div class="news-related">
  <h5 class="head-related">tin TỨC GẦN ĐÂY</h5>
  <div class="news-related-content">
    <?php print render($data['node']); ?>
  </div>
  <div class="pagination">
  <?php print theme('pager');  ?>
  </div>
</div>
<?php endif;?>
