<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/9/2016
 * Time: 9:29 PM
 */
$author = $data['author'];
$gender = $author->field_gender['und'][0]['value'];
$picture = '<img src="'. base_path(). Drupal::theme_path('', 'images/avatar-'.$gender.'.png') . '" />';
if(!empty($author->picture)){
  $style_vars = array(
    'style_name' => 'author_thumb',
    'path' => $author->picture->uri,
    'alt' => '',
    'title' => '',
    'width' => "",
    'height' => '',
    'attributes' => array(
      'class' => array(),
    ),
  );
  $picture = theme_image_style($style_vars);
}
?>
<div class="section-news section-news-detail">
  <div class="container-news">
    <div class="info-author">
      <figure><?php print $picture; ?></figure>
      <div class="content-info-author">
        <h5>tác giả</h5>
        <h3 class="name-author"><?php print Drupal::issetOr($author->field_fullname['und'][0]['value']); ?></h3>
        <span>Số bài viết: <?php print tvu_countAuthorTotalPost($author->uid); ?></span>
        <span>Ngày tham gia: <?php print date('d/m/Y', $author->created) ?></span>
        <span>Email: <?php print $author->mail ?></span>
      </div>
    </div>
    <?php if(Drupal::issetOr($author->field_intro['und'][0]['value'], '')):?>
    <div class="container-news-first">
      <h5 class="header-author">Giới Thiệu Tác Giả</h5>
      <?php print $author->field_intro['und'][0]['value']; ?>
    </div>
    <?php endif;?>
    <?php if(!empty($data['post'])):?>
      <div class="news-related">
        <h5 class="head-related">Bài viết của tác giả <?php print Drupal::issetOr($author->field_fullname['und'][0]['value']) ?></h5>
        <div class="news-related-content">
          <?php
          print render($data['post']); ?>
        </div>
      </div>
      <div class="pagination">
        <?php print theme('pager'); ?>
      </div>
    <?php endif;?>
  </div>
</div>
