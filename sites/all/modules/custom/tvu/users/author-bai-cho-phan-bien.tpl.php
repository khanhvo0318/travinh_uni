<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
?>
<div class="section-account section-author-account">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <div class="btt-projects-list">
    <?php require_once Drupal::module_path("tvu", "users/author-left-menu.tpl.php");?>
    <div class="colum-inbox right-side">
      <div class="btt-projects-list">
        <table>
          <tr class="table-head">
            <th>Tên bài viết</th>
            <th>Nội dung</th>
            <th>Ngày mời<br />phản biện</th>
            <th>Thời hạn gởi<br />phản biện</th>
            <th style="text-align: center;">Trạng thái</th>
            <th>Thao tác</th>
          </tr>
          <?php foreach($data['posts'] as $item) :
            $author = user_load($item->uid);
            $deadline_1 = Drupal::issetOr($item->data_extras['remaining_1'], TVU_REMAINING_1);
            $deadline_2 = Drupal::issetOr($item->data_extras['remaining_2'], TVU_REMAINING_2);
            $wrapper = entity_metadata_wrapper('node', $item);
            $download_url = 'download/projects/node-field_test-'.$item->nid.'-0';
            $timing = $item->changed + ($deadline_1*60*60*24);
            $color = "";
            $current = time();
            if($current >= $item->changed + ($deadline_2*60*60*24)){ // vuot qua thoi han
              $color = 'style="color: #ff0000"';
            } else if($current < $item->changed + ($deadline_2*60*60*24) &&
                      $current > $item->changed + ($deadline_1*60*60*24)
            ){
              $color = 'style="color: #ff9000"';
            } else if($current > $item->changed + (60*60*24)){
              $color = 'style="color: #fff000"';
            }
            $desc = $item->body['und'][0]['value'];
            $desc = drupal_substr($desc, 0, 100);
            ?>
            <tr class="table-body">
              <td class="node-title"><a <?php print $color; ?> href="#"><?php print $item->title; ?></a></td>
              <td style="width: 200px;"><?php print $desc."..."; ?></td>
              <td><?php print date("d/m/Y", $item->changed); ?></td>
              <td><?php print date("d/m/Y", $timing); ?></td>
              <td align="center"><?php print $wrapper->field_status->label(); ?></td>
              <td>
                <a href="<?php print url('process/xem-bai-viet/'.$item->nid); ?>">Phản biện</a> | <a href="<?php print url('process/reject/'.$item->nid); ?>">Từ chối</a>
              </td>
            </tr>
            <?php unset($author); endforeach;?>
        </table>
      </div>
    </div>
  </div>
  <div class="color-note">
    <div class="note-title">* Ghi chú</div>
    <div class="note-item"><span class="nhac-lan-1">Nhắc nhở lần 1</span> </div>
    <div class="note-item"><span class="nhac-lan-2">Nhắc nhở lần 2</span> </div>
    <div class="note-item"><span class="vuot-thoi-gian">Vượt thời gian</span> </div>
    <div class="note-item"><span class="normal">Bài mới gửi</span> </div>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>
