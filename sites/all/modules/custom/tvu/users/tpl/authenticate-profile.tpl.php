<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 10:37 AM
 */
?>
<div class="section-account">
  <h5 class="title-page">Quản lý tài khoản</h5>
  <span class="text-welcome">Xin chào, <a href="javascript:void(0);"><?php print $data['user']->field_fullname['und'][0]['value']; ?></a>
  <span class="mobile-notification"><a href="<?php print url('node/123') ?>"><?php print count($data['notification']) > 0 ? count($data['notification']): ""; ?></a></span>
  </span>
  <div class="table-cms table-my-project-list">
    <div class="table-head">
      <div class="head-child">Bài đã gởi</div>
      <div class="head-child">Ngày phản hồi</div>
      <div class="head-child">Trạng thái</div>
      <div class="head-child">Lượt phản biện</div>
      <div class="head-child">Cập nhật</div>
      <div class="head-child chose-all">Chọn tất cả</div>
    </div>
    <div class="table-main" id="table-my-project-list">
      <?php foreach ($data['node'] as $node) : $wrapper = entity_metadata_wrapper('node', $node);
        $download_url = 'download/projects/node-field_test-'.$node->nid.'-0';
        ?>
        <div class="table-row">
          <div class="main-child"><a href="<?php print url('phan-bien/'.$node->nid); ?>"><?php print $node->title; ?></a></div>
          <div class="main-child"><?php print format_date($node->changed, 'short'); ?></div>
          <div class="main-child"><?php print $wrapper->field_status->label(); ?></div>
          <div class="main-child"><?php print $node->comment_count; ?></div>
          <div class="main-child"><?php print l('Cập nhật', 'cap-nhat-bai/'.$node->nid); ?></div>
          <div class="main-child">
            <div class="custom-checkbox">
              <input value="node-<?php print $node->nid ?>" type="checkbox" name="node-ids" id="bai-<?php print $node->nid ?>" />
              <label for="bai-<?php print $node->nid ?>"></label>
            </div>
          </div>
        </div>
      <?php endforeach;?>
    </div>
    <div class="table-button">
      <a href="#" class="button-delete" id="delete-posts">Xóa</a>
    </div>
  </div>
</div>