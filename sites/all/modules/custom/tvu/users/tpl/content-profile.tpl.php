<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 10:37 AM
 */
?>
<div class="section-account">
  <h5 class="title-page">Danh sách bài gửi chờ phản biện</h5>
  <span class="text-welcome">Xin chào, <a href="javascript:void(0);"><?php print $data['user']->field_fullname['und'][0]['value']; ?></a>
  <span class="mobile-notification"><a href="<?php print url('node/123') ?>"><?php print count($data['notification']) > 0 ? count($data['notification']): ""; ?></a></span>
  </span>
  <div class="table-cms table-my-project-list table-5-column">
    <div class="table-head">
      <div class="head-child">Tiêu đề</div>
      <div class="head-child">Ngày phản hồi</div>
      <div class="head-child">File đính kèm</div>
      <div class="head-child">Trạng thái</div>
      <div class="head-child">Phản biện</div>
    </div>
    <div class="table-main" id="table-my-project-list">
      <?php foreach ($data['node'] as $node) : $wrapper = entity_metadata_wrapper('node', $node);
        $download_url = 'download/projects/node-field_test-'.$node->nid.'-0';
        ?>
        <div class="table-row">
          <div class="main-child"><?php print $node->title; ?></div>
          <div class="main-child"><?php print format_date($node->changed, 'short'); ?></div>
          <div class="main-child"><?php print l('Tải về', $download_url) ?></div>
          <div class="main-child"><?php print $wrapper->field_status->label(); ?></div>
          <div class="main-child"><?php print l('Phản biện', 'phan-bien/'.$node->nid); ?></div>
        </div>
      <?php endforeach;?>
    </div>
  </div>
</div>