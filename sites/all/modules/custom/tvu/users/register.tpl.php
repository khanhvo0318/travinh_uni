<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 11:01 AM
 */
drupal_add_js('https://www.google.com/recaptcha/api.js', 'external');
?>
<h5 class="title-page">ĐĂNG KÝ TÀI KHOẢN</h5>
<div class="section-register">
  <?php
  $form = drupal_get_form('tvu_user_register_form');
  print render($form);
?>
</div>
