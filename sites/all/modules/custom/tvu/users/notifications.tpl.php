<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 10:37 AM
 */
?>
<div class="section-account">
  <h5 class="title-page">Quản lý tài khoản</h5>
  <span class="text-welcome">Xin chào, <a href="javascript:void(0);"><?php print $data['user']->field_fullname['und'][0]['value']; ?></a>
  <span class="mobile-notification"><a><?php print $data['count'] > 0 ? $data['count']: ""; ?></a></span>
  </span>
  <div class="table-cms table-notifications">
    <div class="table-head">
      <div class="head-child">THÔNG BÁO</div>
    </div>
    <div class="table-main" id="table-notification-list">
      <?php foreach ($data['notification'] as $node) :
        ?>
        <div class="table-row">
          <div class="main-child title"><?php print $node->message; ?></div>
          <div class="main-child hour"><?php print date('H:i', $node->created); ?></div>
          <div class="main-child date"><?php print date('d/m/Y', $node->created); ?></div>
        </div>
      <?php endforeach;?>
    </div>
  </div>
</div>