<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
drupal_add_library('system', 'drupal.ajax');
?>
<div class="section-account">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php"); ?>
  <h5 class="title-page">Danh sách bài nghiên cứu</h5>
  <div class="phan-loai-bai-nghien-cuu">
    <div class="phan-loai-item">
      <div class="block-title">Bài chờ đăng</div>
      <div class="btt-projects-list btt-project-approved-list" style="display: block;">
        <table>
          <tr class="table-head">
            <th>Tên bài viết</th>
            <th style="text-align: center;">File đính kèm</th>
            <th>Ngày gửi</th>
            <th>Tác giả</th>
            <th>Thao tác</th>
          </tr>

          <?php foreach ($data['node'] as $item) :
            if($item->field_status['und'][0]['value'] != 1){
              continue;
            }
            $author = user_load($item->uid);
            $author_name = Drupal::issetOr($author->field_profile_fullname["und"][0]["value"], "");
            $reviews = $item->field_reviewer["und"];
            $nhaphanbien = array();
            if (!empty($item->field_reviewer)) {
              foreach ($item->field_reviewer["und"] as $review) {
                $udata         = user_load($review['uid']);
                $name          = Drupal::issetOr($udata->field_profile_fullname["und"][0]["value"], "");
                $nhaphanbien[] = $name;
                unset($udata);
              }
            }
            $deadline_1   = Drupal::issetOr($item->data_extras['remaining_1'], TVU_REMAINING_1);
            $deadline_2   = Drupal::issetOr($item->data_extras['remaining_2'], TVU_REMAINING_2);
            $wrapper      = entity_metadata_wrapper('node', $item);
            $download_url = 'download/projects/node-field_test-' . $item->nid . '-0';
            $timing       = $item->changed + ($deadline_1 * 60 * 60 * 24);
            $color        = "";
            $current      = time();
            if ($current >= $item->changed + ($deadline_2 * 60 * 60 * 24)) { // vuot qua thoi han
              $color = 'style="color: #ff0000"';
            }
            else if ($current < $item->changed + ($deadline_2 * 60 * 60 * 24) &&
                     $current > $item->changed + ($deadline_1 * 60 * 60 * 24)
            ) {
              $color = 'style="color: #ff9000"';
            }
            else if ($current > $item->changed + (60 * 60 * 24)) {
              $color = 'style="color: #fff000"';
            }
            ?>
            <tr class="table-body">
              <td class="node-title"><a <?php print $color; ?>
                  href="<?php print url('process/view-detail/' . $item->nid); ?>"><?php print $item->title; ?></a>
              </td>
              <td align="center"><a
                  href="<?php print url($download_url); ?>"><img
                    src="<?php print base_path() . Drupal::theme_path('', 'images/download-button.png') ?>"/></a>
              </td>
              <td><?php print date("d/m/Y", $item->created); ?></td>
              <td><?php print $author_name; ?></td>
              <td><?php print l("Chọn kỳ đăng", 'process/chon-ky-dang/' . $item->nid, array(
                  'attributes' => array('class' => array('use-ajax') )
                )); ?> | <?php print l("Từ chối", "process/cancelled/".$item->nid) ?></td>
            </tr>
            <?php unset($author); endforeach; ?>
        </table>
      </div>
    </div>
  </div>
</div>
<div id="project-edit-form"></div>
<script>
  addNoSideBarClass();
</script>
