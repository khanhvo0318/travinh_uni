<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/5/2016
 * Time: 2:17 PM
 */
?>
<div class="section-register section-sendfile">
  <h5 class="title-page">Khôi phục mật khẩu</h5>
  <div class="section-item">Vui lòng chọn mật khẩu mới.</div>
  <?php
  $form = drupal_get_form('tvu_password_recover_form', $data);
  print drupal_render($form);
  ?>
</div>
