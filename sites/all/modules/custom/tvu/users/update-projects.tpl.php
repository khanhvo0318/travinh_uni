<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 8:28 PM
 */
?>
<div class="section-register section-sendfile section-post-project">
  <h5 class="title-page">CẬT NHẬT BÀI NGHIÊN CỨU</h5>
  <?php
  if(!user_is_anonymous()){
    $form = drupal_get_form('tvu_update_project_form', $data['node']);
    print drupal_render($form);
  } else {
    print '<div style="padding-bottom: 30px;">Bạn vui lòng đăng nhập trước khi gửi bài nghiên cứu.</div>';
    $form = drupal_get_form('tvu_quick_login_form');
    print drupal_render($form);
  }
  ?>
</div>
