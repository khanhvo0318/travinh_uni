<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 11/14/2016
 * Time: 10:46 AM
 */
$feedback = $data['feedback'];
?>
<div class="section-register section-sendfile section-post-project section-bbt-update-project">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <h5 class="title-page">Bài nghiên cứu chờ phản biện lần 1</h5>
  <div class="section-form">
    <div class="field-title"><?php print $data['node']->title; ?></div>
    <div class="field-desc"><?php print $data['node']->body['und'][0]['value']; ?></div>
    <div class="field-download"><a href="<?php print url('download/projects/node-field_test-'.$data['node']->nid.'-0'); ?>">Tải file đính kèm</a></div>
  </div>
  <?php require_once Drupal::module_path("tvu_process", "/tpl/bbt-project-feedback-block.tpl.php");?>
  <div class="section-action">
    <?php
      $form = drupal_get_form('tvu_process_project_status_3_form', $data['node']);
      print drupal_render($form);
    ?>
  </div>
</div>
<script>addNoSideBarClass();</script>
