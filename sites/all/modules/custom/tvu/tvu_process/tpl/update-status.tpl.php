<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 11/14/2016
 * Time: 11:22 AM
 */
$nid = $data['nid'];
$form = drupal_get_form('project_update_status_form', $nid);
print '<div id="project-edit-form"><div class="form-content">'. drupal_render($form).'</div></div>';
?>
<script>

  (jQuery)(function ($){
    $(document).ready(function (){
      showLightbox('Cập nhật trạng thái');
      $("#edit-status").on("change", function (){
        if($(this).val() == "-1"){
          $("#reject-reason").removeClass("element-invisible");
        } else {
          $("#reject-reason").removeClass("element-invisible");
        }
      });
      $("#project-update-status-form").on("submit", function (){
        if($("#reject-reason").hasClass("element-invisible")){ // not display
          return true;
        } else {
          $reason = $("#edit-lydo").val();
          if($reason.trim() == ""){
            $("#edit-lydo").parents(".form-item").find(".error").remove();
            $html = '<div class="messages error messages-inline">Vui lòng nhập lý do từ chối.</div>';
            $("#edit-lydo").parents(".form-item").append($html);
            return false;
          }
        }
        return true;
      });
    });
  });
</script>