<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
$node = $data['node'];
$feedbacks = $data['feedback'];
$user = $data['user'];
?>
<div class="section-account section-author-account section-xem-phan-bien">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <div class="btt-projects-list">
    <?php require_once Drupal::module_path("tvu", "users/author-left-menu.tpl.php");?>
    <div class="colum-inbox right-side">
      <div class="phanbien-list">
        <div class="update"><a href="<?php print url('process/gui-phan-bien/'.$node->nid); ?>">Gửi phản biện</a></div>
        <div class="node-title">
          <span>Tiêu đề: </span><br />
          <?php print $node->title; ?>
        </div>
        <div class="node-summary">
          <div class="node-content">
            <span>Nội dung: </span><br /><br />
            <?php print $node->body["und"][0]['value']; ?>
          </div>
          <div class="node-download">
            <span>File đinh kèm: </span><br /><br />
            <a href="<?php print url("download/projects/node-field_test-".$node->nid."-0"); ?>"><img src="<?php print base_path().Drupal::theme_path('', 'images/download-button.png') ?>" /></a>
          </div>
        </div>
        <?php foreach ($feedbacks as $item):
          $score = field_collection_item_load($item->field_score_collection['und'][0]['value']);
          $author = "Ban biên tập gởi phản biện";
          if($item->uid == $user->uid){
            $author = "Phản biện của bạn";
          }
          ?>
          <div class="phanbien-item">
            <div class="title"><?php print $author; ?> <span class="date"><?php print date("d/m/Y", $item->created) ?></span> </div>
            <div class="content">
              <p><?php print $score->field_danhgiachung["und"][0]['value']; ?></p>
              <?php if(!empty($item->field_attached)): ?>
              <div class="download-file">
                <span>File đính kèm</span><br /><br />
                <a href="<?php print file_create_url($item->field_attached["und"][0]['uri']) ?>"><img src="<?php print base_path().Drupal::theme_path('', 'images/download-button.png') ?>" /></a>
              </div>
              <?php endif;?>
            </div>
          </div>
        <?php endforeach;?>
      </div>
    </div>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>