<?php
function tvu_process_email_setting($form, &$form_state){
  $form['config'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Mời người phản biện'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['config']['tvu_invite_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_invite_subject', NULL),
    '#groups' => 'config',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_invite_content', $defaults);
  $form['config']['tvu_invite_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'config',
  );

  $form['xacnhandangbai'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Xác nhận đăng bài viết'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['xacnhandangbai']['tvu_approved_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_approved_subject', NULL),
    '#groups' => 'xacnhandangbai',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_approved_content', $defaults);
  $form['xacnhandangbai']['tvu_approved_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'xacnhandangbai',
  );

  $form['tuchoidangbai'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Từ chối đăng bài viết'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['tuchoidangbai']['tvu_rejected_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_rejected_subject', NULL),
    '#groups' => 'tuchoidangbai',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_rejected_content', $defaults);
  $form['tuchoidangbai']['tvu_rejected_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'tuchoidangbai',
  );

  $form['xacnhanguibai'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Xác nhận gửi bài viết'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['xacnhanguibai']['tvu_new_post_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_new_post_subject', NULL),
    '#groups' => 'xacnhanguibai',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_new_post_content', $defaults);
  $form['xacnhanguibai']['tvu_new_post_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'xacnhanguibai',
  );

  $form['thongbaiguibai'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Thông báo thư ký có bài mới'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['thongbaiguibai']['tvu_notification_post_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_notification_post_subject', NULL),
    '#groups' => 'thongbaiguibai',
  );
  $form['thongbaiguibai']['tvu_notification_post_email'] = array(
    '#type' => 'textfield',
    '#title' => "Email nhận thông báo",
    '#default_value' => variable_get('tvu_notification_post_email', NULL),
    '#groups' => 'thongbaiguibai',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_notification_post_content', $defaults);
  $form['thongbaiguibai']['tvu_notification_post_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'thongbaiguibai',
  );

  $form['thongbaochinhlan1'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Thông báo tác giả chỉnh sửa bài'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['thongbaochinhlan1']['tvu_chinhsualan1_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_chinhsualan1_subject', NULL),
    '#groups' => 'thongbaochinhlan1',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_chinhsualan1_content', $defaults);
  $form['thongbaochinhlan1']['tvu_chinhsualan1_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'thongbaochinhlan1',
  );

//  $form['thongbaochinhlan2'] = array(
//    '#type'        => 'fieldset',
//    '#title'       => t('Thông báo tác giả chỉnh sửa lần 2'),
//    '#collapsible' => TRUE,
//    '#collapsed' => TRUE,
//  );
//  $form['thongbaochinhlan2']['tvu_chinhsualan2_subject'] = array(
//    '#type' => 'textfield',
//    '#title' => "Tiêu đề",
//    '#default_value' => variable_get('tvu_chinhsualan2_subject', NULL),
//    '#groups' => 'thongbaochinhlan2',
//  );
//  $defaults = array(
//    'value' => '',
//    'format' => filter_default_format(),
//  );
//  $my_richtext_field = variable_get('tvu_chinhsualan2_content', $defaults);
//  $form['thongbaochinhlan2']['tvu_chinhsualan2_content'] = array(
//    '#type' => 'text_format',
//    '#title' => "Nội dung email",
//    '#default_value' => $my_richtext_field['value'],
//    '#groups' => 'thongbaochinhlan2',
//  );

  $form['thongbaophanbien'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Thông báo nhà phản biện gửi phản biện'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['thongbaophanbien']['tvu_guiphanbien_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_guiphanbien_subject', NULL),
    '#groups' => 'thongbaophanbien',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_guiphanbien_content', $defaults);
  $form['thongbaophanbien']['tvu_guiphanbien_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'thongbaophanbien',
  );

  $form['bbthinhthuc'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Thông báo ban biên tập hình thức'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bbthinhthuc']['tvu_bbthinhthuc_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_bbthinhthuc_subject', NULL),
    '#groups' => 'bbthinhthuc',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_bbthinhthuc_content', $defaults);
  $form['bbthinhthuc']['tvu_bbthinhthuc_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'bbthinhthuc',
  );

  $form['btttienganh'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Thông báo ban biên tập tiếng anh'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['btttienganh']['tvu_btttienganh_subject'] = array(
    '#type' => 'textfield',
    '#title' => "Tiêu đề",
    '#default_value' => variable_get('tvu_btttienganh_subject', NULL),
    '#groups' => 'btttienganh',
  );
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $my_richtext_field = variable_get('tvu_btttienganh_content', $defaults);
  $form['btttienganh']['tvu_btttienganh_content'] = array(
    '#type' => 'text_format',
    '#title' => "Nội dung email",
    '#default_value' => $my_richtext_field['value'],
    '#groups' => 'btttienganh',
  );
  return system_settings_form($form);
}
?>