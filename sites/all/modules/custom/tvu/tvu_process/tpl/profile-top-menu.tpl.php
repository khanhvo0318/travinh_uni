<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/20/2016
 * Time: 8:16 PM
 */
global $user;
?>
<div class="profile-top-menu">
  <ul class="menu-list">
    <li class="menu-item"><?php print l("Trang cá nhân", "user/".$user->uid, array("attributes" => array("class" => array("profile-link")))); ?></li>
    <li class="menu-item"><?php print l("Gửi bài nghiên cứu", "gui-bai-nghien-cuu", array("attributes" => array("class" => array("profile-link")))); ?></li>
    <li class="menu-item">
      <?php print l("Thông tin cá nhân", 'user/view-profile', array("attributes" => array("class" => array("profile-link")))); ?>
    </li>
    <?php if(tvu_checkRole($user->roles, 4)):?>
      <li class="menu-item">
        <?php print l("Admin Page", 'admin/articles-manager', array("attributes" => array("class" => array("profile-link")))); ?>
      </li>
    <?php endif;?>
    <li class="menu-item"><?php print l("Thông báo", "user/thong-bao", array("attributes" => array("class" => array("profile-link")))); ?></li>
  </ul>
</div>
