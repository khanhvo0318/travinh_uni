<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
$profile = $data['user'];
$node = $data['node'];
$feedback = $data['feedback'];
$node_revision = node_revision_list($node);
?>
<div class="section-account-information section-project-detail-page">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <h5 class="title-page">Thông tin bài nghiên cứu</h5>
  <div class="profile-information project-detail-information section-bbt-update-project">
<!--    <div class="tong-hop-phan-bien">--><?php //print l("Tổng hợp phản biện", 'process/tong-hop-phan-bien/'.$node->nid); ?><!--</div>-->
    <div class="field-title">
      <span>Tiêu đề:</span> <?php print $node->title; ?>
    </div>
    <div class="field-desc"><?php print $node->body['und'][0]['value']; ?></div>
    <div class="field-download"><a href="<?php print url('download/projects/node-field_test-'.$node->nid.'-0'); ?>">Tải file đính kèm</a></div>
    <?php if(!empty($node_revision)): ?>
      <div class="revision-list">
        <div class="revision-title-head">Lịch sử chỉnh sửa bài</div>
        <div class="revision-block-list">
          <?php $index = count($node_revision)-1; foreach($node_revision as $revision):
            $revisionData = node_load($data["node"]->nid, $revision->vid);
            if(!empty($revision->current_vid)):
              ?>
              <div class="revision-item">
                <div class="revision-title">Phiên bản hiện tại</div>
                <div class="revision-date">Ngày tạo: <?php print date("d/m/Y", $revision->timestamp); ?></div>
                <div class="revision-download"><a href="<?php print url('process/revision/download/'.$data['node']->nid.'/'.$revisionData->vid); ?>">Tải về</a></div>
              </div>
            <?php elseif($revisionData->nid == $data['node']->nid): ?>
              <div class="revision-item">
                <div class="revision-title">Phiên bản gốc</div>
                <div class="revision-date">Ngày tạo: <?php print date("d/m/Y", $revision->timestamp); ?></div>
                <div class="revision-download"><a href="<?php print url('process/revision/download/'.$data['node']->nid.'/'.$revisionData->vid); ?>">Tải về</a></div>
              </div>
            <?php else : ?>
              <div class="revision-item">
                <div class="revision-title">Phiên bản thứ <?php print $index; ?></div>
                <div class="revision-date">Ngày tạo: <?php print date("d/m/Y", $revision->timestamp); ?></div>
                <div class="revision-download"><a href="<?php print url('process/revision/download/'.$data['node']->nid.'/'.$revisionData->vid); ?>">Tải về</a></div>
              </div>
            <?php endif; ?>
            <?php $index--; endforeach; ?>
        </div>
      </div>
    <?php endif; ?>
    <div class="feedback-list">
      <?php foreach ($feedback as $item) :
        $score = field_collection_item_load($item->field_score_collection['und'][0]['value']);
        $quality = field_collection_item_load($item->field_quality_collection['und'][0]['value']);
        $wrapper = entity_metadata_wrapper('node', $item);
        $author = user_load($item->uid);
        $author_name = Drupal::issetOr($author->field_profile_fullname["und"][0]["value"], "");
        ?>
        <div class="feedback-item">
          <div class="feedback-title">
            <div class="sender"><?php print $author_name; ?></div>
            <div class="date"><?php print date("d/m/Y", $item->created); ?></div>
          </div>
          <div class="feedback-content">
            <span>Tiêu đề:</span><br /><br />
            <p><?php print $item->title ; ?></p>
          </div>
          <div class="feedback-content">
            <span>Các kết quả mới có giá trị khoa học của bài viết:</span><br /><br />
            <p><?php print !empty($item->body["und"][0]["value"]) ? $item->body["und"][0]["value"] : "N/A" ; ?></p>
          </div>
          <div class="feedback-content">
            <span>Nhận xét:</span><br /><br />
            <p><?php print $quality->field_feedback_content["und"][0]['value']; ?></p>
          </div>
          <div class="feedback-content">
            <span>Nội dung chỉnh sửa:</span><br /><br />
            <p><?php print !empty($item->field_noidungchinhsua["und"][0]["value"]) ? $item->field_noidungchinhsua["und"][0]["value"] : "N/A" ; ?></p>
          </div>
          <div class="feedback-content">
            <span>Cho điểm:</span><br /><br />
            <span>Mức độ phù hợp: </span><?php print $score->field_mucdophuhop["und"][0]['value']." Điểm"; ?><br />
            <span>Tính độc đáo: </span><?php print $score->field_tinhdocdao["und"][0]['value']." Điểm"; ?><br />
            <span>Độ sâu chuyên môn: </span><?php print $score->field_dosauchuyenmon["und"][0]['value']." Điểm"; ?><br />
            <span>Hình thức trình bày: </span><?php print $score->field_hinhthuc["und"][0]['value']." Điểm"; ?><br />
            <span>Đánh giá chung:</span><br /><br />
            <p><?php print $score->field_danhgiachung["und"][0]['value']; ?></p>
          </div>
          <div class="feedback-kl">
            <span>Kết luận:</span><br /><br />
            <?php print $wrapper->field_ketluan->label(); ?>
          </div>
          <?php if (!empty($item->field_attached)): ?>
            <div class="renew-feedback" style="padding-top: 10px;">
              <span>File đính kèm:</span><br /><br />
              <a href="<?php print file_create_url( $item->field_attached["und"][0]["uri"] ); ?>">Tải về</a>
            </div>
          <?php endif; ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>
