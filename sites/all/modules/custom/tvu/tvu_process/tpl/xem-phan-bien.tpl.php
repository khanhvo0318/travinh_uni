<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
$node = $data['node'];
$feedbacks = $data['feedback'];
?>
<div class="section-account section-author-account section-xem-phan-bien">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <div class="btt-projects-list">
    <?php require_once Drupal::module_path("tvu", "users/author-left-menu.tpl.php");?>
    <div class="colum-inbox right-side">
      <div class="phanbien-list">
        <div class="update"><a href="<?php print url('cap-nhat-bai/'.$node->nid); ?>">Gửi bài đã chỉnh</a></div>
        <div class="node-title">
          <span>Tiêu đề: </span><br />
          <?php print $node->title; ?>
        </div>
        <?php foreach ($feedbacks as $item):
          $score = field_collection_item_load($item->field_score_collection['und'][0]['value']);
          $wrapper = entity_metadata_wrapper('node', $item);
          ?>
        <div class="phanbien-item">
          <div class="title">Ban biên tập gởi phản biện <span class="date"><?php print date("d/m/Y", $item->created) ?></span> </div>
          <div class="content">
            <div class="">
              <p><?php print $score->field_danhgiachung["und"][0]['value']; ?></p>
            </div>
            <div class="">
              <span>Kết luận: </span>
              <p><?php print $wrapper->field_ketluan->label(); ?></p>
            </div>
            <?php if(!empty($item->field_attached)): ?>
              <div class="">
                <div class="attached-inner">
                  <span>File đính kèm</span><br /><br />
                 <a href="<?php print file_create_url($item->field_attached["und"][0]['uri']) ?>"><img src="<?php print base_path().Drupal::theme_path('', 'images/download-button.png') ?>" /></a>
                </div>
              </div>
            <?php endif;?>
          </div>
        </div>
        <?php endforeach;?>
      </div>
    </div>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>