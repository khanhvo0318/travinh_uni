<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
$node = $data['node'];
?>
<div class="section-account-information section-project-detail-page section-tonghopphanbien">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <h5 class="title-page">gửi phản biện bài nghiên cứu</h5>
  <div class="profile-information project-detail-information">
    <div class="field-title">
      <span>Tiêu đề:</span> <?php print $node->title; ?>
    </div>
    <div class="gui-phan-bien-form">
      <?php
      $form = drupal_get_form('tvu_process_gui_phan_bien_form', $node);
      print drupal_render($form);
      ?>
    </div>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>
