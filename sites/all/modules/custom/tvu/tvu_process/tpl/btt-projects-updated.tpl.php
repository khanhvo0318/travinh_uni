<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 3:30 PM
 */
?>
<div class="section-register section-sendfile section-post-project section-bbt-update-project">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <h5 class="title-page">Cập nhật bài nghiên cứu</h5>
  <?php
    $form = drupal_get_form('tvu_process_project_updated_form', $data['node']);
    print drupal_render($form);
  ?>
</div>
<script>addNoSideBarClass();</script>
