<?php
function tvu_processGetFeedbackList($node_id, $uid=0){
  $query = new ZMEntityQuery();
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'feedback')
        ->propertyCondition('status', NODE_PUBLISHED);
  $query->fieldCondition('field_project', 'nid', $node_id, "=");
  $query->propertyOrderBy("created", "ASC");
  if($uid !== 0){
    $query->propertyCondition('uid', $uid, "IN");
  }
  $result = $query->execute();
  if(!empty($result['node'])){
    $nids = array_keys($result['node']);
    $nodes = node_load_multiple($nids);
    return $nodes;
  }
  return "";
}

/**
 * Project status 0.
 */
function tvu_process_project_status_0_form($form, &$form_state, $node) {
  global $user;
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
//  $form['reason'] = array(
//    '#type'              => 'textfield',
//    '#title'             => t('Lý do từ chối đăng bài:'),
//    '#prefix'            => '<div style="margin-bottom: 10px;">',
//    '#suffix'            => '</div>',
//  );
  $form['chose_feedback'] = array(
    '#type'  => 'submit',
    '#value' => t('Chọn ban biên tập'),
    '#ajax' => array(
      'callback' => 'project_chose_editor',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  if(tvu_checkRole($user->roles, TVU_THU_KY)){
    $form['approved'] = array(
      '#type'  => 'submit',
      '#value' => t('Đăng luôn'),
      '#submit' => array('project_approved'),
      '#attributes' => array(
        'class' => array(
          'project-submit-button'
        )
      )
    );
//    $form['reject'] = array(
//      '#type'  => 'submit',
//      '#value' => t('Từ chối'),
//      '#submit' => array('project_reject'),
//      '#attributes' => array(
//        'class' => array(
//          'project-submit-button'
//        )
//      )
//    );
    $form['update'] = array(
      '#type'  => 'submit',
      '#value' => t('Cập nhật trạng thái'),
      '#ajax' => array(
        'callback' => 'project_update_status',
        'wrapper' => 'project-edit-form',
        'method' => 'replace',
      ),
      '#attributes' => array(
        'class' => array(
          'project-submit-button'
        )
      )
    );
  }
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

/**
 * approved bài nghien cuu
 */
function project_approved($form, $form_state){
  global $user;
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $nodeData->field_status['und'][0]['value'] = 1;
    node_save($nodeData);
  }
  drupal_set_message("Bài nghiên cứu đã được duyệt đăng", "status");
  drupal_goto("user/".$user->uid);
}

/**
 * tu choi dang bài nghien cuu
 */
function project_reject($form, $form_state){
  global $user;
  $nid = $form_state['input']['nid'];
  $reason = $form_state['input']['reason'];
//  dpm($form_state);
  if(trim($reason) == ""){
    form_set_error("reason", "Nhập lý do.");
  }
  if($nodeData = node_load($nid)){
    $nodeData->field_status['und'][0]['value'] = "-1";
    $nodeData->field_lydo['und'][0]['value'] = $reason;
    node_save($nodeData);
    $data = array(
      'uid' => $nodeData->uid,
      'message' => 'Ban biên tập/Thư ký vừa từ chối đăng bài bài nghiên cứu "'.$nodeData->title.'" của bạn với lý do: "'. $reason .'"',
      'created' => REQUEST_TIME,
      'updated' => REQUEST_TIME
    );
    drupal_write_record('tvu_notification', $data);
    // email to this user
    drupal_set_message("Bài nghiên cứu đã bị từ chối đăng.", "status");
    drupal_goto("user/".$user->uid);
  }
}

/**
 * chose reviews
 * @param $form
 * @param $form_state
 * @return mixed
 */
function project_chose_reviews($form, $form_state){
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $data = array(
      'nid' => $nodeData->nid
    );
    $content = Drupal::render_module_template("tvu_process", "tpl/chose-review.tpl.php", $data);
    $commands = array(
      ajax_command_replace('#project-edit-form', $content),
    );
    $replace = array('#type' => 'ajax', '#commands' => $commands);
    return  $replace;
  }
}

function project_chose_editor($form, $form_state){
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $data = array(
      'nid' => $nodeData->nid
    );
    $content = Drupal::render_module_template("tvu_process", "tpl/chose-editor.tpl.php", $data);
    $commands = array(
      ajax_command_replace('#project-edit-form', $content),
    );
    $replace = array('#type' => 'ajax', '#commands' => $commands);
    return  $replace;
  }
}


/**
 * chose reviews
 * @param $form
 * @param $form_state
 * @return mixed
 */
function project_update_status($form, $form_state){
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $data = array(
      'nid' => $nodeData->nid
    );
    $content = Drupal::render_module_template("tvu_process", "tpl/update-status.tpl.php", $data);
    $commands = array(
      ajax_command_replace('#project-edit-form', $content),
    );
    $replace = array('#type' => 'ajax', '#commands' => $commands);
    return  $replace;
  }
}

function project_chose_review_form($form, $form_state, $nid){
  $reviewers = array();
  $node = node_load($nid);
  foreach($node->field_reviewer["und"] as $r){
    $reviewers[$r['uid']] = $r['uid'];
  }
  $form['#tree'] = TRUE;
  if (empty($form_state['num_invite'])) {
    $form_state['num_invite'] = 1;
  }
  $form['#action'] = url('process/update-review');
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['review'] = array(
    '#type'          => 'checkboxes',
    '#default_value' => $reviewers,
    '#options'       => tvu_getAuthenticateUser(),
    '#prefix' => '<div class="field-register"><div class="label">Chọn người phản biện:</div>',
    '#suffix' => '</div>',
  );
  $form['invite'] = array(
    '#markup' => '<div class="field-register"><div class="label">Mời người phản biện:</div>
                    <div class="invite-group">
                      <div class="invite-item">
                        <div class="form-item field-register"><div class="label">Họ tên:</div>
                         <input type="text" name="invite_name[]" />
                         </div>
                         <div class="form-item field-register"><div class="label">Email:</div>
                         <input type="text" name="invite_mail[]" />
                         </div>
                       </div>
                    </div>
                    <div class="add-new" id="invite-new-reviews"><a href="#">Thêm người phản biện</a></div>
                </div>'
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}

function project_chose_editor_form($form, $form_state, $nid){
  $reviewers = array();
  $node = node_load($nid);
  foreach($node->field_editor["und"] as $r){
    $reviewers[$r['uid']] = $r['uid'];
  }
  $form['#action'] = url('process/update-editor');
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['review'] = array(
    '#type'          => 'checkboxes',
    '#default_value' => $reviewers,
    '#options'       => tvu_getBBTUser(TVU_BBT),
    '#prefix' => '<div class="field-register"><div class="label">Chọn ban biên tập:</div>',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}

function process_new_invite_submit($form, &$form_state){
  if (!isset($form_state['num_invite'])) {
    $form_state['num_invite'] = 1;
    $form_state['num_invite']++;
  }
  $form_state['num_invite']++;
  $form_state['rebuild'] = TRUE;
}

function process_new_invite_callback($form, &$form_state){
  return $form['invite']['group'];
}

/**
 * @param $form
 * @param $form_state
 * @param #nid
 * @return mixed
 */
function project_update_status_form($form, $form_state, $nid){
  $fields = field_info_field("field_status");
  $allowed_values = list_allowed_values($fields);
  $reviewers = array();
  $node = node_load($nid);
  foreach($node->field_reviewer["und"] as $r){
    $reviewers[$r['uid']] = $r['uid'];
  }
  $form['#action'] = url('process/update-status');
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['status'] = array(
    '#type'   => 'select',
    '#default_value' => $node->field_status["und"][0]["value"],
    '#empty_option'  => t('- Chọn trạng thái -'),
    '#options'       => $allowed_values,
    '#prefix' => '<div class="field-register">',
    '#suffix' => '</div>'
  );
  $form['lydo'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Lý do'),
    '#prefix'        => '<div id="reject-reason" class="element-invisible">',
    '#suffix'        => '</div>',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}
/**
 * Project status 0.
 */
function tvu_process_project_status_3_form($form, &$form_state, $node) {
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
  $form['chose_feedback'] = array(
    '#type'  => 'submit',
    '#value' => t('Chọn lại phản biện'),
    '#ajax' => array(
      'callback' => 'project_chose_reviews',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['extend'] = array(
    '#type'  => 'submit',
    '#value' => t('Gia hạn thời gian'),
    '#ajax' => array(
      'callback' => 'project_chose_extend',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật trạng thái'),
    '#ajax' => array(
      'callback' => 'project_update_status',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function project_chose_extend($form, $form_state){
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $data = array(
      'nid' => $nodeData->nid
    );
    $content = Drupal::render_module_template("tvu_process", "tpl/chose-extend.tpl.php", $data);
    $commands = array(
      ajax_command_replace('#project-edit-form', $content),
    );
    $replace = array('#type' => 'ajax', '#commands' => $commands);
    return  $replace;
  }
}


function project_chose_extend_form($form, $form_state, $nid){
  $reviewers = array();
  $node = node_load($nid);
  foreach($node->field_reviewer["und"] as $r){
    $reviewers[$r['uid']] = $r['uid'];
  }
  $form['#action'] = url('process/update-extend');
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['remaining_1'] = array(
    '#type'          => 'textfield',
    '#prefix' => '<div class="section-item field-reviewers">
							<div class="label">Thời hạn nhắc nhở lần 1 (ngày):</div>
							<div class="field-register">',
    '#suffix' => '</div>
						</div>',
    '#attributes' => array(
      'size' => 30,
      'class' => array(
        'input-number-only'
      )
    ),
    '#default_value' => Drupal::issetOr($node->data_extras['remaining_1'], TVU_REMAINING_1),
  );
  $form['remaining_2'] = array(
    '#type'          => 'textfield',
    '#prefix' => '<div class="section-item field-reviewers">
							<div class="label">Thời hạn nhắc nhở lần 2 (ngày):</div>
							<div class="field-register">',
    '#suffix' => '</div>
						</div>',
    '#attributes' => array(
      'size' => 30,
      'class' => array(
        'input-number-only'
      )
    ),
    '#default_value' => Drupal::issetOr($node->data_extras['remaining_2'], TVU_REMAINING_2),
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}


function tvu_process_project_status_4_form($form, $form_state, $node){
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
  $form['status'] = array(
    '#type'  => 'hidden',
    '#value' => $node->field_status['und'][0]['value'],
  );
  $form['reason'] = array(
    '#type'              => 'textfield',
    '#title'             => t('Lý do từ chối đăng bài:'),
    '#prefix'            => '<div style="margin-bottom: 10px;">',
    '#suffix'            => '</div>',
  );
  $form['send_back_author'] = array(
    '#type'  => 'submit',
    '#value' => t('Gửi tác giả chỉnh sửa'),
    '#submit' => array('send_back_author'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['approved'] = array(
    '#type'  => 'submit',
    '#value' => t('Đăng luôn'),
    '#submit' => array('project_approved'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
//  $form['reject'] = array(
//    '#type'  => 'submit',
//    '#value' => t('Từ chối'),
//    '#submit' => array('project_reject'),
//    '#attributes' => array(
//      'class' => array(
//        'project-submit-button'
//      )
//    )
//  );
  $form['summary'] = array(
    '#type'  => 'submit',
    '#value' => t('Tổng hợp phản biện'),
    '#ajax' => array(
      'callback' => 'project_tonghopphanbien',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật trạng thái'),
    '#ajax' => array(
      'callback' => 'project_update_status',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function send_back_author($form, $form_state){
  global $user;
  $nid = $form_state['input']['nid'];
  $status = $form_state['input']['status'];
  if($node = node_load($nid)){
    if($status == 4){ // chuyển trang thái về chờ phản biện lần 1
      $node->field_status['und'][0]['value'] = 5;
    } else if($status == 8) {
      $node->field_status['und'][0]['value'] = 9;
    }
    node_save($node);
    $data = array(
      'uid' => $node->uid,
      'message' => 'Ban biên tập/Thư ký vừa gửi yêu câu chỉnh sửa bài nghiên cứu "'.$node->title.'" của bạn',
      'created' => REQUEST_TIME,
      'updated' => REQUEST_TIME
    );
    drupal_write_record('tvu_notification', $data);
  }
  drupal_set_message("Bài nghiên cứu đã gửi lại tác giả chỉnh sửa", "status");
  drupal_goto("user/".$user->uid);
}

function project_tonghopphanbien($form, $form_state){
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $data = array(
      'nid' => $nodeData->nid
    );
    $content = Drupal::render_module_template("tvu_process", "tpl/tong-hop-phan-bien-form.tpl.php", $data);
    $commands = array(
      ajax_command_replace('#project-edit-form', $content),
    );
    $replace = array('#type' => 'ajax', '#commands' => $commands);
    return  $replace;
  }
}

function project_tong_hop_phan_bien_form($form, $form_state, $nid){
  global $array_score;
  global $ketluan;
  $form['#action'] = url("process/admin/tong-hop-phan-bien");
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['body'] = array(
    '#type'          => 'textarea',
    '#title' => t('Các kết quả mới có giá trị khoa học của bài viết'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['feedback_content']= array(
    '#type'          => 'textarea',
    '#title' => t('Nhận xét phần “Tóm tắt nội dung bài viết” của tác giả'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['bocuc']= array(
    '#type'          => 'textarea',
    '#title' => t('Bố cục trình bày các kết quả khoa học (văn phong, chính tả và bố cục trình bày)'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['trichdan']= array(
    '#type'          => 'textarea',
    '#title' => t('Trích dẫn nguồn tài liệu tham khảo'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['tienganh']= array(
    '#type'          => 'textarea',
    '#title' => t('Trình độ sử dụng thành thạo tiếng Anh'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['cachthuc']= array(
    '#type'          => 'textarea',
    '#title' => t('Cách trình bày các công thức, ký hiệu, bảng, biểu, hình vẽ'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['noidungchinhsua']= array(
    '#type'          => 'hidden',
    '#title' => t('Những Nội dung bài viết cần chỉnh sửa hoặc bổ sung làm rõ'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['mucdophuhop'] = array(
    '#type'   => 'select',
//    '#empty_option'  => t('- Cho điểm -'),
    '#options'       => $array_score,
    '#title' => t('Mức độ phù hợp với phạm vi của Tạp chí'),
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',

  );$form['tinhdocdao'] = array(
    '#type'   => 'select',
//    '#empty_option'  => t('- Cho điểm -'),
    '#options'       => $array_score,
    '#title' => t('Tính độc đáo/tính mới của bài viết'),
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',

  );$form['dosauchuyenmon'] = array(
    '#type'   => 'select',
//    '#empty_option'  => t('- Cho điểm -'),
    '#options'       => $array_score,
    '#title' => t('Độ sâu về chuyên môn '),
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',

  );$form['hinhthuc'] = array(
    '#type'   => 'select',
//    '#empty_option'  => t('- Cho điểm -'),
    '#options'       => $array_score,
    '#title' => t('Hình thức trình bày bài viết '),
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',

  );
  $form['danhgiachung']= array(
    '#type'          => 'textarea',
    '#title' => t('Đánh giá chung về bài viết'),
    '#required'      => FALSE,
    '#format'        => "plain_text",
    '#rows' => 5,
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['ketluan'] = array(
    '#type'   => 'select',
    '#empty_option'  => t('- Kết luận -'),
    '#options'       => $ketluan,
    '#title' => t('Kết luận'),
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',

  );
  $form['attached'] = array(
    '#type'   => 'file',
    '#title' => t('File đính kèm'),
    '#description' => t('Chấp nhận kiểu file: doc, docx, pdf, zip, rar, txt'),
    '#prefix' => '<div class="section-item">',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Gửi phản biện'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}

function tvu_process_project_status_5_form($form, $form_state, $node){
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
  $form['extend'] = array(
    '#type'  => 'submit',
    '#value' => t('Gia hạn thời gian'),
    '#ajax' => array(
      'callback' => 'project_chose_extend',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
//  $form['reject'] = array(
//    '#type'  => 'submit',
//    '#value' => t('Từ chối'),
//    '#submit' => array('project_reject'),
//    '#attributes' => array(
//      'class' => array(
//        'project-submit-button'
//      )
//    )
//  );
  $form['chuyen-phan-bien'] = array(
    '#type'  => 'submit',
    '#value' => t('Chuyển phản biện'),
    '#submit' => array('project_chuyen_phan_bien'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['approved'] = array(
    '#type'  => 'submit',
    '#value' => t('Đăng luôn'),
    '#submit' => array('project_approved'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật trạng thái'),
    '#ajax' => array(
      'callback' => 'project_update_status',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );

  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function project_chuyen_phan_bien($form, $form_state){
  global $user;
  $nid = $form_state['input']['nid'];
  if($node = node_load($nid)){
    // cap nhat trang thai bai viet thanh cho phan bien lan 2
    $node->field_status['und'][0]['value'] = 7;
    node_save($node);
    module_invoke_all("project_update_phan_biên", $node);
  }
  drupal_set_message("Bài nghiên cứu đã được chuyển phản biện.", "status");
  drupal_goto("user/".$user->uid);
}

function tvu_process_project_status_7_form($form, $form_state, $node){
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
  $form['extend'] = array(
    '#type'  => 'submit',
    '#value' => t('Gia hạn thời gian'),
    '#ajax' => array(
      'callback' => 'project_chose_extend',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật trạng thái'),
    '#ajax' => array(
      'callback' => 'project_update_status',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function tvu_process_project_status_8_form($form, $form_state, $node){
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
  $form['status'] = array(
    '#type'  => 'hidden',
    '#value' => $node->field_status['und'][0]['value'],
  );
//  $form['reason'] = array(
//    '#type'              => 'textfield',
//    '#title'             => t('Lý do từ chối đăng bài:'),
//    '#prefix'            => '<div style="margin-bottom: 10px;">',
//    '#suffix'            => '</div>',
//  );
  $form['send_back_author'] = array(
    '#type'  => 'submit',
    '#value' => t('Gửi tác giả chỉnh sửa'),
    '#submit' => array('send_back_author'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['approved'] = array(
    '#type'  => 'submit',
    '#value' => t('Đăng luôn'),
    '#submit' => array('project_approved'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
//  $form['reject'] = array(
//    '#type'  => 'submit',
//    '#value' => t('Từ chối'),
//    '#submit' => array('project_reject'),
//    '#attributes' => array(
//      'class' => array(
//        'project-submit-button'
//      )
//    )
//  );
  $form['summary'] = array(
    '#type'  => 'submit',
    '#value' => t('Tổng hợp phản biện'),
    '#ajax' => array(
      'callback' => 'project_tonghopphanbien',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật trạng thái'),
    '#ajax' => array(
      'callback' => 'project_update_status',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function tvu_process_project_status_9_form($form, $form_state, $node){
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
//  $form['reason'] = array(
//    '#type'              => 'textfield',
//    '#title'             => t('Lý do từ chối đăng bài:'),
//    '#prefix'            => '<div style="margin-bottom: 10px;">',
//    '#suffix'            => '</div>',
//  );
  $form['extend'] = array(
    '#type'  => 'submit',
    '#value' => t('Gia hạn thời gian'),
    '#ajax' => array(
      'callback' => 'project_chose_extend',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
//  $form['reject'] = array(
//    '#type'  => 'submit',
//    '#value' => t('Từ chối'),
//    '#submit' => array('project_reject'),
//    '#attributes' => array(
//      'class' => array(
//        'project-submit-button'
//      )
//    )
//  );
  $form['bbt-hinhthuc'] = array(
    '#type'  => 'submit',
    '#value' => t('Chuyển ban biên tập hình thức'),
    '#ajax' => array(
      'callback' => 'project_chose_bbt_hinh_thuc',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function project_chose_bbt_hinh_thuc($form, $form_state){
  $nid = $form_state['input']['nid'];
  if($nodeData = node_load($nid)){
    $data = array(
      'nid' => $nodeData->nid
    );
    $content = Drupal::render_module_template("tvu_process", "tpl/chose-bbt-hinh-thuc.tpl.php", $data);
    $commands = array(
      ajax_command_replace('#project-edit-form', $content),
    );
    $replace = array('#type' => 'ajax', '#commands' => $commands);
    return  $replace;
  }
}

function project_chose_bbt_hinhthuc_form($form, $form_state, $nid){
  $bbt_ht_arr = array();
  $node = node_load($nid);
  foreach($node->field_editor_print["und"] as $r){
    $bbt_ht_arr[$r['uid']] = $r['uid'];
  }
  $form['#action'] = url('process/update-bbt-hinhthuc');
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['review'] = array(
    '#type'          => 'checkboxes',
    '#default_value' => $bbt_ht_arr,
    '#options'       => tvu_getBBTUser(TVU_BBT_HT),
    '#prefix' => '<div class="field-register"><div class="label">Chọn ban biên tập hình thức:</div>',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}

function tvu_process_project_status_10_form($form, $form_state, $node){
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $node->nid,
  );
  $form['approved'] = array(
    '#type'  => 'submit',
    '#value' => t('Đăng luôn'),
    '#submit' => array('project_approved'),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['bbt-hinhthuc'] = array(
    '#type'  => 'submit',
    '#value' => t('Chọn ban biên tập hình thức'),
    '#ajax' => array(
      'callback' => 'project_chose_bbt_hinh_thuc',
      'wrapper' => 'project-edit-form',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'class' => array(
        'project-submit-button'
      )
    )
  );
  $form['#suffix'] = '<div id="project-edit-form"></div>';
  return $form;
}

function project_chon_ky_dang_form($form, $form_state, $nid){
  $node = node_load($nid);
  $form['#action'] = url('process/update-ky-dang');
  $form['nid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid,
  );
  $form['review'] = array(
    '#type'          => 'textfield',
    '#default_value' => @$node->field_kydang["und"][0]['value'],
    '#prefix' => '<div class="field-register"><div class="label">Kỳ đăng:</div>',
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Cập nhật'),
    '#prefix' => '<div class="section-item-submit">',
    '#suffix' => '</div>',
  );
  return $form;
}