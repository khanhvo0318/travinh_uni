<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 8/2/2016
 * Time: 8:21 PM
 */
global $user;
$node = $data['node'];
$comments = $data['comments'];
?>
<div class="section-news section-news-detail">
  <div class="container-news">
    <h5 class="title-news-first"><?php print $node->title; ?></h5>
    <div class="container-news-first">
      <?php print $node->body['und'][0]['value']; ?>
    </div>
    <?php if(!empty($comments)):?>
    <div class="news-related">
      <h5 class="head-related">Danh sách phản biện</h5>
      <div class="news-related-content">
        <?php foreach($comments as $item):
            $comment = comment_load($item);
            $attached = _tvu_load_comment_file_attached($comment->cid);
          ?>
          <div class="news-related-content" style="padding-bottom: 30px;">
            <div class="title-related"><?php print $comment->subject; ?></div>
            <span class="time-related"><?php print $comment->name. ' - '. date("d/m/Y H:i", $comment->created) ?></span>
            <div class="content-related container-news-first" style="padding-top: 20px;"><p><?php print $comment->comment_body['und'][0]['value'] ?></p></div>
            <?php if($attached != 0):
              $file = file_load($attached);
              ?>
              <div class="content-attached"><a href="<?php print file_create_url($file->uri); ?>">File đính kèm</a> </div>
            <?php endif;  ?>
          </div>
        <?php endforeach;?>
      </div>
    </div>
    <?php endif;?>
    <?php if($node->uid != $user->uid):?>
    <div class="news-related section-register comment-review-form section-register section-sendfile section-post-project">
      <h5 class="head-related">Gửi phản biện</h5>
      <?php
        $reviews = drupal_get_form('tvu_review_form');
        print drupal_render($reviews);
      ?>
    </div>
    <?php endif;?>
  </div>
</div>
