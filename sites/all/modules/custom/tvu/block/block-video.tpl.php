<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/31/2016
 * Time: 3:01 PM
 */
?>
<div class="sidebar-item sidebar-video">
  <h5 class="header-sidebar">VIDEO</h5>
  <?php foreach ($data['video'] as $item):
    $videoCode = _tvu_getYoutubeVideoCode($item->field_youtube_link['und'][0]['value']);
    ?>
  <div class="sidebar-video-content">
    <iframe width="272" height="182" src="https://www.youtube.com/embed/<?php print $videoCode ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe>
  </div>
  <?php endforeach;?>
</div>
