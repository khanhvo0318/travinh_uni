<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/31/2016
 * Time: 5:24 PM
 */
?>
<div class="sidebar-item sidebar-news">
  <h5 class="header-sidebar">CÁC BÀI VIẾT/ TÁC GIẢ <br/>ĐƯỢC QUAN TÂM NHIỀU NHẤT</h5>
  <ul class="list-news">
    <?php foreach($data['node'] as $item): ?>
      <li><a href="<?php print url('node/'.$item->nid); ?>"><?php print $item->title; ?></a></li>
    <?php endforeach;?>
  </ul>
</div>
