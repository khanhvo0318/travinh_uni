<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/31/2016
 * Time: 3:01 PM
 */
?>
<div class="sidebar-item sidebar-photo">
  <h5 class="header-sidebar">PHOTOS</h5>
  <?php foreach ($data['gallery'] as $item) :
    $style_vars = array(
      'style_name' => 'gallery_thumb',
      'path' => $item->field_image['und'][0]['uri'],
      'alt' => $item->title,
      'title' => $item->title,
      'width' => "",
      'height' => '',
      'attributes' => array(
        'class' => array(),
      ),
    );
    $cover = theme_image_style($style_vars);
    ?>
  <div class="sidebar-photo-content">
    <?php print $cover; ?>
    <p class="desc-photo"><?php print $item->title ?>.<span><?php print strip_tags($item->body['und'][0]['safe_value']) ?></span></p>
  </div>
  <?php endforeach;?>
</div>
