<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/7/2016
 * Time: 10:05 AM
 */
?>
<div class="table-cms table-2-column">
  <div class="table-main">
    <?php foreach ($data['node'] as $item):
      ?>
      <div class="table-row">
        <div class="main-child"><?php print $item->title; ?></div>
        <div class="main-child"><a href="<?php print url(file_create_url($item->field_file_download['und'][0]['uri'])); ?>" class="link-down">Tải về</a></div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
