<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 2:41 PM
 */
?>
<?php if(!empty($data['node'])):?>
<div class="news-related">
  <h5 class="head-related">tin tức gần đây</h5>
  <div class="news-related-content">
    <?php print render($data['node']); ?>
  </div>
  <div class="pagination">
    <?php print theme('pager', array('element' => 1));  ?>
  </div>
</div>
<?php endif;?>