<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/31/2016
 * Time: 1:30 PM
 */
?>
<div class="sidebar-item sidebar-magazine">
  <h5 class="header-sidebar">TẠP CHÍ KHOA HỌC</h5>
  <div class="slide-magazine owl-carousel">
    <?php foreach ($data['magazine'] as $item):
      $style_vars = array(
        'style_name' => 'magazine_thumb',
        'path' => $item->field_image['und'][0]['uri'],
        'alt' => $item->title,
        'title' => $item->title,
        'width' => "",
        'height' => '',
        'attributes' => array(
          'class' => array(),
        ),
      );
      $cover = theme_image_style($style_vars);
      ?>
      <div class="item">
        <a href="<?php print url('node/'.$item->nid); ?>" class="link-download"><?php print $cover; ?></a>
        <div class="content">
          <span class="text">Số</span>
          <span class="number"><?php print $item->field_number['und'][0]['value']; ?></span>
								<span class="time">
									<span class="txt">Phát hành ngày: </span>
									<span class="date"><?php print date("d/m/Y", $item->field_publish_date['und'][0]['value']) ?></span>
								</span>
        </div>
        <a href="<?php print url('node/'.$item->nid); ?>" class="link-download">Chi Tiết</a>
      </div>
    <?php endforeach;?>
  </div>
</div>
