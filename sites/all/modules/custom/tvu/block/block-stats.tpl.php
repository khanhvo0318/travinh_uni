<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/8/2016
 * Time: 10:47 AM
 */
?>
<div class="section-gallery">
  <div class="container-gallery">
    <ul class="block-grid block-grid-3">
      <?php foreach ($data['node'] as $item) :
        $style_vars = array(
          'style_name' => 'magazine_thumb',
          'path' => $item->field_image['und'][0]['uri'],
          'alt' => $item->title,
          'title' => $item->title,
          'width' => "",
          'height' => '',
          'attributes' => array(
            'class' => array(),
          ),
        );
        $cover = theme_image_style($style_vars);
        ?>
        <li>
          <a href="<?php print url("node/".$item->nid); ?>" class="image-gallery"><?php print $cover; ?></a>
          <a href="#" class="name-gallery"><?php print $title; ?></a>
          <span class="time-gallery">Tháng <?php print date("m/Y", $item->field_publish_date['und'][0]['value']) ?></span>
        </li>
      <?php endforeach; ?>
    </ul>
    <div class="pagination">
      <?php print theme('pager');  ?>
    </div>
  </div>
</div>
