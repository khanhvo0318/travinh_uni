<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/6/2016
 * Time: 2:32 PM
 */
?>
<div class="table-cms">
  <div class="table-head">
    <div class="head-child">Tác giả</div>
    <div class="head-child">Email</div>
    <div class="head-child">Số bài đăng</div>
  </div>
  <div class="table-main">
    <?php foreach($data as $item):
      $author = user_load($item->uid);
      $slug_name = pathauto_cleanstring(Drupal::issetOr($author->field_fullname['und'][0]['value'], ''));
      ?>
    <div class="table-row">
      <div class="main-child"><a href="<?php print url('thong-tin-tac-gia/'.$author->uid.'/'.$slug_name); ?>"><?php print Drupal::issetOr($author->field_fullname['und'][0]['value'], ''); ?></a></div>
      <div class="main-child"><?php print $author->mail; ?></div>
      <div class="main-child"><?php print tvu_countAuthorTotalPost($author->uid); ?></div>
    </div>
    <?php unset($author); endforeach;?>
  </div>
</div>
