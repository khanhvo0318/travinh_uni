<?php
/**
 * auto generate string base on length passed
 * @param int $length
 *
 * @return string
 */
function tvu_autoGenerateString($length = 6){
  $characters = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
  $randstring = '';
  for ($i = 0; $i < $length; $i++) {
    $randstring = $characters[rand(0, strlen($characters))];
  }
  return $randstring;
}

/**
 * created new user
 * @param $account
 * @return mixed
 */
function autoCreateNewUser($account, $node){
  $password = tvu_autoGenerateString(6);
  $newUser  = array(
    'name'           => $account['email'],
    'pass'           => $password,
    'mail'           => $account['email'],
    'status'         => 1,
    'timezone'       => 'Asia/Ho_Chi_Minh',
    'init'           => $account['email'],
    'field_profile_fullname' => array('und' => array('0' => array('value' => $account['fullname']))),
  );
  $account['password'] = $password;
  $account['node_title'] = $node->title;
  $account['timing'] = $node->change + (TVU_REMAINING_1*60*60*24);
  if(!user_load_by_name($newUser['name'])){
    $user = user_save(NULL, $newUser);
    module_invoke_all('invite_new_user', $account);
    return $user;
  } else {
    return user_load_by_name($newUser['name']);
  }
}

function tvu_checkIsWebAdmin($roles, $array_needle = array('webmaster', 'contents')){
  foreach ($roles as $r) {
    if (in_array($r, $array_needle)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * check user has roles specification
 * @param $roles
 * @param $checked
 *
 * @return bool
 */
function tvu_checkRole($roles, $checked){
  $role_keys = array_keys($roles);
  if(in_array($checked, $role_keys)){
    return true;
  }
  return false;
}
?>