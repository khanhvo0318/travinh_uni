<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
$profile = $data['user'];
?>
<div class="section-account-information">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <h5 class="title-page">Cật nhật thông tin cá nhân</h5>
  <div class="section-register profile-information section-update-profile">
    <?php
      $form = drupal_get_form("tvu_users_update_profile_form", $profile);
      print drupal_render($form);
    ?>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>
