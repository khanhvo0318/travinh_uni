<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
$profile = $data['user'];
?>
<div class="section-account-information">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php");?>
  <h5 class="title-page"><?php print Drupal::issetOr($profile->field_profile_fullname['und'][0]["value"], ""); ?></h5>
  <div class="profile-information">
    <div class="profile-item">
      <span>Email: </span><?php print $profile->mail; ?>
    </div>
    <div class="profile-item">
      <span>Bằng cấp/hồ sơ nghiên cứu: </span><br /><br />
      <?php print $profile->field_intro['und'][0]['value']; ?>
    </div>
    <div class="profile-item">
      <span>Cơ quan công tác: </span><?php print Drupal::issetOr($profile->field_job["und"][0]['value'], ""); ?>
    </div>
    <div class="profile-item">
      <span>Điện thoại: </span><?php print Drupal::issetOr($profile->field_phone["und"][0]['value'], ""); ?>
    </div>
    <div class="profile-item">
      <a href="<?php print url("user/update-profile") ?>" class="submit-button">Chỉnh sửa</a>
    </div>
  </div>
</div>
<script>
  addNoSideBarClass();
</script>
