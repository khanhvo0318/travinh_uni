<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 9/19/2016
 * Time: 12:48 PM
 */
drupal_add_library('system', 'drupal.ajax');
?>
<div class="section-account">
  <?php require_once Drupal::module_path("tvu_process", "/tpl/profile-top-menu.tpl.php"); ?>
  <h5 class="title-page">Thông báo từ hệ thống</h5>
  <div class="phan-loai-bai-nghien-cuu message-list thong-bao-he-thong">
    <?php foreach ($data['notification'] as $item): ?>
    <div class="message-item" style="margin-top: 10px;"><?php print $item->message; ?> (<?php print date("d/m/Y", $item->created) ?>)</div>
    <?php endforeach; ?>
  </div>
  <div class="color-note">
    <div class="note-title">* Ghi chú</div>
    <div class="note-item"><span class="nhac-lan-1">Nhắc nhở lần 1</span> </div>
    <div class="note-item"><span class="nhac-lan-2">Nhắc nhở lần 2</span> </div>
    <div class="note-item"><span class="vuot-thoi-gian">Vượt thời gian</span> </div>
    <div class="note-item"><span class="normal">Bài mới gửi</span> </div>
  </div>
</div>
<div id="project-edit-form"></div>
<script>
  addNoSideBarClass();
</script>
