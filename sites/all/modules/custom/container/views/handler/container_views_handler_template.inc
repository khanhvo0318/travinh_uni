<?php

/**
 * @file
 * Definition of container_views_handler_template.
 */

/**
 * Views area text handler.
 *
 * @ingroup views_area_handlers
 */
class container_views_handler_template extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();
    $options['container_template'] = array('default' => "");
    $options['template_variables'] = array('default' => array());
    $options['tokenize'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['container_template'] = array(
      '#title' => t('Container Template'),
      '#type' => 'textfield',
      '#default_value' => $this->options['container_template'],
      '#description' => t('Path to container template, support token {theme}, {module-modulename}'),
    );

    $form['var_0'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Variable 1'),
      '#default_value' => isset($this->options['template_variables'][0]) ? $this->options['template_variables'][0] : "",
    );

    $form['var_1'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Variable 2'),
      '#default_value' => isset($this->options['template_variables'][1]) ? $this->options['template_variables'][1] : "",
    );

    $form['var_2'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Variable 3'),
      '#default_value' => isset($this->options['template_variables'][2]) ? $this->options['template_variables'][2] : "",
    );

    // @TODO: Refactor token handling into a base class.
    $form['tokenize'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use replacement tokens from the first row'),
      '#default_value' => $this->options['tokenize'],
    );

    // Get a list of the available fields and arguments for token replacement.
    $options = array();
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
    }

    $count = 0; // This lets us prepare the key as we want it printed.
    foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
      $options[t('Arguments')]['%' . ++$count] = t('@argument title', array('@argument' => $handler->ui_name()));
      $options[t('Arguments')]['!' . $count] = t('@argument input', array('@argument' => $handler->ui_name()));
    }

    if (!empty($options)) {
      $output = '<p>' . t('The following tokens are available. If you would like to have the characters \'[\' and \']\' please use the html entity codes \'%5B\' or  \'%5D\' or they will get replaced with empty space.' . '</p>');
      foreach (array_keys($options) as $type) {
        if (!empty($options[$type])) {
          $items = array();
          foreach ($options[$type] as $key => $value) {
            $items[] = $key . ' == ' . check_plain($value);
          }
          $output .= theme('item_list',
            array(
              'items' => $items,
              'type' => $type
            ));
        }
      }

      $form['token_help'] = array(
        '#type' => 'fieldset',
        '#title' => t('Replacement patterns'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#value' => $output,
        '#id' => 'edit-options-token-help',
        '#dependency' => array(
          'edit-options-tokenize' => array(1),
        ),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
    }
  }

  function options_submit(&$form, &$form_state) {
    $vars = array();
    for ($i = 0; $i < 3; $i++) {
      $vars[] = $form_state['values']['options']['var_' . $i];
    }
    $form_state['values']['options']['template_variables'] = $vars;

    parent::options_submit($form, $form_state);
  }

  function render($empty = FALSE) {
    if (!empty($this->options['container_template'])) {
      if ($this->options['tokenize']) {
        foreach ($this->options['template_variables'] as $key => $value) {
          $this->options['template_variables'][$key] = $this->view->style_plugin->tokenize_value($value, 0);
        }
      }

      $template = DrupalContainer::real_path($this->options['container_template']);
      $vars['params']     = $this->options['template_variables'];

      return DrupalContainer::render_template($template, $vars);
    }
    return '';
  }
}
