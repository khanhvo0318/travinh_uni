<?php


$plugin = array(
  'title' => t('Container: URL query access'),
  'description' => t('Control access by the url query path.'),
  'callback' => 'container_access_get_query_access_check',
  'settings form' => 'container_access_get_query_access_settings',
  'summary' => 'container_access_get_query_access_summary',
  'default' => array('visibility_setting' => 0, 'query' => ''),
);


/**
 * Settings form
 */
function container_access_get_query_access_settings($form, &$form_state, $conf) {
  $form['settings']['note'] = array(
    '#value' => '<div class="description">' . t('Note: if no context is chosen, the current page path will be used.') . '</div>',
  );

  $form['settings']['visibility_setting'] = array(
    '#type' => 'radios',
    '#options' => array(
      0 => t('Only one criteria must pass.'),
      1 => t('All criteria must pass.'),
    ),
    '#default_value' => $conf['visibility_setting'],
  );

  $form['settings']['query'] = array(
    '#type' => 'textarea',
    '#title' => t('Queries'),
    '#default_value' => $conf['query'],
    '#description' => t("Enter one query per line."),
  );

  return $form;
}

/**
 * Check for access.
 */
function container_access_get_query_access_check($conf, $context) {
  $request_uri = request_uri();
  $page_match = FALSE;

  if ($conf['visibility_setting']) {
    foreach (explode("\n", $conf['query']) as $path) {
      if (strpos($request_uri, $path) !== FALSE) {
        $page_match = TRUE;
      }
      else {
        $page_match = FALSE;
        break;
      }
    }
  }
  else {
    foreach (explode("\n", $conf['query']) as $path) {
      if (strpos($request_uri, $path) !== FALSE) {
        $page_match = TRUE;
        break;
      }
    }
  }

  return $page_match;
}

/**
 * Provide a summary description.
 */
function container_access_get_query_access_summary($conf, $context) {
  $paths = array();
  foreach (explode("\n", $conf['query']) as $path) {
    $paths[] = check_plain($path);
  }

  if ($conf['visibility_setting']) {
    return t("Url query must have all values of @values", array('@values' => implode(",", $paths)));
  }
  else {
    return t("Url query must have 1 value of @values", array('@values' => implode(",", $paths)));
  }
}