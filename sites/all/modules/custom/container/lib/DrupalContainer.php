<?php
/**
 * Created by PhpStorm.
 * User: ZM
 * Date: 7/26/2014
 * Time: 10:27 PM
 */
require_once "Pimple/Container.php";
require_once "Pimple/ServiceProviderInterface.php";

class DrupalContainer {
  static private $_registry = array();

  protected static $container;

  /**
   * Sets a new global container.
   *
   * @param \Pimple\Container $container
   */
  public static function setContainer(\Pimple\Container $container = NULL) {
    static::$container = $container;
  }

  /**
   * Returns the currently active global container.
   *
   *
   * @return \Pimple\Container|null
   */
  public static function getContainer() {
    if (!static::$container) {
      static::$container = new \Pimple\Container();
    }

    return static::$container;
  }

  /**
   * Gets the current active user.
   */
  public static function currentUser() {
    return $GLOBALS['user'];
  }

  /**
   * Gererater URL.
   *
   * @param $route_name
   * @param array $route_parameters
   * @param array $options
   *
   * @return string
   */
  public static function url($route_name, $route_parameters = array(), $options = array()) {
    $options['query'] = $route_parameters;

    return url($route_name, $options);
  }

  /**
   * Gererater link.
   *
   * @param $text
   * @param $route_name
   * @param array $route_parameters
   * @param array $options
   *
   * @return string
   */
  public static function l($text, $route_name, array $route_parameters = array(), array $options = array()) {
    $options['query'] = $route_parameters;
    if ( ! isset( $options['html'] )) {
      $options['html'] = TRUE;
    }

    return l($text, $route_name, $options);
  }


  public static function l_class($text, $route_name, $class = '', array $options = array()) {
    if (!empty($class)) {
      $options['attributes']['class'][] = $class;
    }
    if ( ! isset( $options['html'] )) {
      $options['html'] = TRUE;
    }

    return l($text, $route_name, $options);
  }

  /**
   * Return themed image.
   *
   * @param $path
   * @param array $options
   *
   * @return string
   * @throws Exception
   */
  public static function image($path, $options = array(), $class = '') {
    $image = array(
      'path' => $path,
    );
    if(!empty($class)){
      $options['attributes']['class'][] = $class;
    }

    return theme('image', $image + $options);
  }


  /**
   * Return themed image style.
   *
   * @param $path
   * @param $style_name
   * @param array $options
   *
   * @return string
   * @throws Exception
   */
  public static function image_style($path, $style_name, $options = array()) {
    $image = array(
      'path'       => $path,
      'style_name' => $style_name,
    );

    return theme('image_style', $image + $options);
  }

  /**
   * Generate image link.
   *
   * @param $path
   * @param $route_name
   * @param array $route_parameters
   * @param array $options
   *
   * @return string
   */
  public static function link_image($path, $route_name, array $route_parameters = array(), array $options = array()) {
    $image           = self::image($path, $options);
    $options['html'] = TRUE;

    return self::l($image, $route_name, $route_parameters, $options);
  }

  /**
   * Drupal static store for one request.
   *
   * @param $key
   * @param $set_data
   */
  public static function drupal_static($key, $set_data = NULL){
    $data = &drupal_static($key);

    if ($set_data) {
      $data = $set_data;
    }
    else {
      return $data;
    }
  }

  /**
   * Fast render template to html markup.
   *
   * @param $template
   * @param $data
   */
  public static function render_template($template, $data = array()) {
    $render_array = static::template($template, $data);
    return render($render_array);
  }

  /**
   * Create template array ready for render.
   *
   * @param $template
   * @param $data
   */
  public static function template($template, $data = array(), $template_id = NULL) {
    if (!self::endsWith($template, '.tpl.php')) {
      $template = $template . ".tpl.php";
    }
    return array(
      'container_template' => array(
        '#theme'       => 'container_template',
        '#template'    => $template,
        '#data'        => $data,
        '#template_id' => $template_id,
      )
    );
  }
  public static function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
  }
  /**
   * Get path of module and file in module.
   *
   * @param $module
   * @param $file
   *
   * @return string
   */
  public static function module_path($module, $file = NULL) {
    static $path = NULL;
    if ( ! isset( $path[$module] )) {
      $path[$module] = drupal_get_path('module', $module);
    }
    if ($file) {
      return $path[$module] . "/" . $file;
    }

    return $path[$module];
  }

  /**
   * Get current active theme name.
   *
   * @return string
   */
  public static function theme_name() {
    return $GLOBALS['conf']['theme_default'];
  }
  /**
   * Get path of theme and file in theme.
   *
   * @param $theme
   * @param $file
   *
   * @return string
   */
  public static function theme_path($theme = NULL, $file = NULL) {
    if (!$theme) {
      $theme = self::theme_name();
    }

    static $path = NULL;
    if ( ! isset( $path[$theme] )) {
      $path[$theme] = drupal_get_path('theme', $theme);
    }
    if ($file) {
      return $path[$theme] . "/" . $file;
    }

    return $path[$theme];
  }

  /**
   * Add css to Drupal.
   *
   * @param $css
   *  Path to file or array of path.
   * @param array $options
   */
  public static function add_css($css, $options = array()) {
    if (is_array($css)) {
      foreach ($css as $item) {
        drupal_add_css($item, $options);
      }
    }
    else {
      drupal_add_css($css, $options);
    }
  }

  /**
   * Add js to Drupal.
   *
   * @param $css
   *  Path to file or array of path.
   * @param array $options
   */
  public static function add_js($js, $options = array()) {
    if (is_array($js)) {
      foreach ($js as $item) {
        drupal_add_js($item, $options);
      }
    }
    else {
      drupal_add_js($js, $options);
    }
  }

  /**
   * Get real path.
   *
   * @param $path
   *
   * @return mixed|string
   */
  public static function real_path($path) {
    return container_realpath($path);
  }

  /**
   * Implement dpm.
   *
   * @param $vars
   * @param string $title
   */
  public static function dpm($vars, $name = NULL){
    if (function_exists('dpm')) {
      dpm($vars, $name);
    }
  }

  /**
   * Implement dpm one.
   *
   * @param $vars
   * @param $name
   */
  public static function dpm_one($vars, $name = NULL){
    $key = isset( $name ) ? "dpm__" . $name : "dpm__one";
    if ( ! DrupalContainer::drupal_static($key)) {
      static::dpm($vars, $name);
      DrupalContainer::drupal_static($key, TRUE);
    }
  }

  /**
   * Register a new variable
   *
   * @param string $key
   * @param mixed $value
   * @param bool $override
   */
  public static function register($key, $value, $override = false)
  {
    if (isset(self::$_registry[$key])) {
      if (!$override) {
        return;
      }
    }
    self::$_registry[$key] = $value;
  }

  /**
   * Unregister a variable from register by key
   *
   * @param string $key
   */
  public static function unregister($key)
  {
    if (isset(self::$_registry[$key])) {
      if (is_object(self::$_registry[$key]) && (method_exists(self::$_registry[$key], '__destruct'))) {
        self::$_registry[$key]->__destruct();
      }
      unset(self::$_registry[$key]);
    }
  }

  /**
   * Retrieve a value from registry by a key
   *
   * @param string $key
   * @return mixed
   */
  public static function registry($key)
  {
    if (isset(self::$_registry[$key])) {
      return self::$_registry[$key];
    }
    return null;
  }

  /**
   * Return data if isset or default.
   *
   * @param            $var
   * @param bool|FALSE $default
   *
   * @return bool
   */
  public static function issetOr(&$var, $default = false) {
    return !empty($var) ? $var : $default;
  }

  /**
   * Render template place in module.
   *
   * @param $module
   * @param $template
   * @param $variables
   *
   * @return string
   */
  public static function render_module_template($module, $template, $variables = array()) {
    $template_path = drupal_get_path("module", $module) . "/" . $template;
    return self::render_template($template_path, $variables);
  }

  /**
   * Render template place in current theme.
   *
   * @param $template
   * @param $variables
   *
   * @return string
   */
  public static function render_theme_template($template, $variables) {
    $template_path = drupal_get_path("module", self::theme_name()) . "/" . $template;
    return self::render_template($template_path, $variables);
  }

  public static function link_active_by_query($text, $path, array $options = array()){
    global $language_url;
    static $use_theme = NULL;
    static $query_parameters = NULL;

    if (!$query_parameters) {
      $query_parameters = drupal_get_query_parameters();
    }

    if (isset($options['query'])) {
      $active = FALSE;
      foreach ($options['query'] as $key => $value) {
        if (isset($query_parameters[$key]) && $query_parameters[$key] == $value) {
          $active = TRUE;
        }else{
          break;
        }
      }

      if ($active) {
        $options['attributes']['class'][] = 'active';
      }
    }
    else {
      if (empty($query_parameters)) {
        $options['attributes']['class'][] = 'active';
      }
    }
    // Merge in defaults.
    $options += array(
      'attributes' => array(),
      'html' => FALSE,
    );

    // Append active class.
    if (($path == $_GET['q'] || ($path == '<front>' && drupal_is_front_page())) &&
      (empty($options['language']) || $options['language']->language == $language_url->language)) {
      $options['attributes']['class'][] = 'link-active';
    }

    // Remove all HTML and PHP tags from a tooltip. For best performance, we act only
    // if a quick strpos() pre-check gave a suspicion (because strip_tags() is expensive).
    if (isset($options['attributes']['title']) && strpos($options['attributes']['title'], '<') !== FALSE) {
      $options['attributes']['title'] = strip_tags($options['attributes']['title']);
    }

    // Determine if rendering of the link is to be done with a theme function
    // or the inline default. Inline is faster, but if the theme system has been
    // loaded and a module or theme implements a preprocess or process function
    // or overrides the theme_link() function, then invoke theme(). Preliminary
    // benchmarks indicate that invoking theme() can slow down the l() function
    // by 20% or more, and that some of the link-heavy Drupal pages spend more
    // than 10% of the total page request time in the l() function.
    if (!isset($use_theme) && function_exists('theme')) {
      // Allow edge cases to prevent theme initialization and force inline link
      // rendering.
      if (variable_get('theme_link', TRUE)) {
        drupal_theme_initialize();
        $registry = theme_get_registry(FALSE);
        // We don't want to duplicate functionality that's in theme(), so any
        // hint of a module or theme doing anything at all special with the 'link'
        // theme hook should simply result in theme() being called. This includes
        // the overriding of theme_link() with an alternate function or template,
        // the presence of preprocess or process functions, or the presence of
        // include files.
        $use_theme = !isset($registry['link']['function']) || ($registry['link']['function'] != 'theme_link');
        $use_theme = $use_theme || !empty($registry['link']['preprocess functions']) || !empty($registry['link']['process functions']) || !empty($registry['link']['includes']);
      }
      else {
        $use_theme = FALSE;
      }
    }
    if ($use_theme) {
      return theme('link', array('text' => $text, 'path' => $path, 'options' => $options));
    }
    // The result of url() is a plain-text URL. Because we are using it here
    // in an HTML argument context, we need to encode it properly.
    return '<a href="' . check_plain(url($path, $options)) . '"' . drupal_attributes($options['attributes']) . '>' . ($options['html'] ? $text : check_plain($text)) . '</a>';
  }
}

class Drupal extends DrupalContainer{

}