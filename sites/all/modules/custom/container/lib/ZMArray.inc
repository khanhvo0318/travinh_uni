<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 21/10/15
 * Time: 1:45 PM
 */

class ZMArray {

  public static function getNestedValue($item, array $parents, &$key_exists = NULL) {
    $ref = $item;
    foreach ($parents as $parent) {
      if (is_array($ref) && array_key_exists($parent, $ref)) {
        $ref = &$ref[$parent];
      }
      elseif (is_object($ref) && property_exists($ref, $parent)) {
        $ref = $ref->$parent;
      }
      elseif (is_object($ref) && method_exists($ref, '__get') && $ref->__get($parent) !== NULL) {
        // Support objects that override the __get magic method.
        // This also doesn't support if $ref->$parent exists but is set to NULL.
        $ref = $ref->$parent;
      }
      else {
        $key_exists = FALSE;
        return NULL;
      }
    }
    $key_exists = TRUE;
    return $ref;
  }

  public static function filterByNestedValue(array $items, array $parents, $value) {
    $return = array();
    foreach ($items as $key => $item) {
      $key_exists = FALSE;
      $found_value = static::getNestedValue($item, $parents, $key_exists);
      if ($key_exists) {
        if (is_array($value) && in_array($found_value, $value)) {
          $return[$key] = $item;
        }
        elseif ($found_value == $value) {
          $return[$key] = $item;
        }
      }
    }
    return $return;
  }

  public static function extractNestedValuesToArray(array $items, array $value_parents, array $key_parents = NULL) {
    $return = array();
    foreach ($items as $index => $item) {
      $key = isset($key_parents) ? static::getNestedValue($item, $key_parents) : $index;
      $return[$key] = static::getNestedValue($item, $value_parents);
    }
    return $return;
  }

  private static function array_to_xml($data_arr, &$xml_data) {
    foreach($data_arr as $key => $value) {
      if(is_array($value)) {
        if(!is_numeric($key)){
          $subnode = $xml_data->addChild("$key");
          self::array_to_xml($value, $subnode);
        }
        else{
          self::array_to_xml($value, $xml_data);
        }
      }
      else {
        $xml_data->addChild("$key")->addCData("$value");
      }
    }
  }

  public static function array2xml($data_arr, $root = 'data'){
    $xml_data = new SimpleXMLExtended("<?xml version=\"1.0\"?><{$root}></{$root}>");
    self::array_to_xml($data_arr,$xml_data);
    return $xml_data->asXML();
  }

}
/**
 * Extend SompleZMLElement to create CDATA for node
 */
class SimpleXMLExtended extends SimpleXMLElement {
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this);
    $no   = $node->ownerDocument;
    $node->appendChild($no->createCDATASection($cdata_text));
  }
}
