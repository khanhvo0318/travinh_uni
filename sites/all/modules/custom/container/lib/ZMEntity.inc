<?php

/**
 * Created by PhpStorm.
 * User: martin
 * Date: 21/10/15
 * Time: 1:44 PM
 */
require_once 'ZMArray.inc';
/**
 * ZMEntity
 */
class ZMEntity{
  /**
   * A wrapper around entity_load() to load a single entity by ID.
   *
   * @param string $entity_type
   *   The entity type of $entity.
   * @param int $entity_id
   *   The ID of the entity to load.
   *
   * @return object
   *   The entity object, or FALSE on failure.
   *
   * @see entity_load()
   */
  public static function load($entity_id, $entity_type = 'node') {
    $entities = entity_load($entity_type, array($entity_id));
    return reset($entities);
  }

  /**
   * Load a single entity revision.
   *
   * @param string $entity_type
   *   The entity type of $entity.
   * @param int $entity_id
   *   The ID of the entity to load.
   * @param int $revision_id
   *   The ID of the revision to load.
   *
   * @return object
   *   The entity object of the specific revision, or FALSE on failure.
   *
   * @see entity_load()
   */
  public static function loadRevision($entity_type, $entity_id, $revision_id) {
    $conditions = array();
    if ($revision_key = static::entityTypeHasProperty($entity_type, array('entity keys', 'revision'))) {
      $conditions[$revision_key] = $revision_id;
    }
    $entities = entity_load($entity_type, array($entity_id), $conditions);
    return reset($entities);
  }

  public static function loadByCondition($entity_type, array $conditions) {
    $entities = entity_load($entity_type, FALSE, $conditions);
    return reset($entities);
  }

  public static function entityTypeHasProperty($entity_type, array $parents) {
    if ($info = entity_get_info($entity_type)) {
      return drupal_array_get_nested_value($info, $parents);
    }
  }

  /**
   * Remove the empty field values from an entity.
   *
   * We run this on migrations because empty field values are only removed when
   * an entity is submitted via the UI and forms, and not programmatically.
   *
   * @param string $entity_type
   *   An entity type.
   * @param object $entity
   *   An entity object.
   */
  public static function removeEmptyFieldValues($entity_type, $entity) {
    // Invoke field_default_submit() which will filter out empty field values.
    $form = $form_state = array();
    _field_invoke_default('submit', $entity_type, $entity, $form, $form_state);
  }

  /**
   * Remove invalid field value deltas from an entity.
   *
   * @param string $entity_type
   *   An entity type.
   * @param object $entity
   *   An entity object.
   */
  public static function removeInvalidFieldDeltas($entity_type, $entity) {
    list(, , $bundle) = entity_extract_ids($entity_type, $entity);
    $instances = field_info_instances($entity_type, $bundle);
    foreach (array_keys($instances) as $field_name) {
      if (!empty($entity->{$field_name})) {
        $field = field_info_field($field_name);
        if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
          foreach ($entity->{$field_name} as $langcode => $items) {
            if (count($items) > $field['cardinality']) {
              $entity->{$field_name}[$langcode] = array_slice($items, 0, $field['cardinality']);
            }
          }
        }
      }
    }
  }

  /**
   * A lightweight version of entity save for field values only.
   *
   * @param string $entity_type
   *   The entity type of $entity.
   * @param object $entity
   *   The entity object to update.
   * @throws InvalidArgumentException
   */
  public static function updateFieldValues($entity_type, $entity) {
    list($id) = entity_extract_ids($entity_type, $entity);
    if (empty($id)) {
      throw new InvalidArgumentException(t('Cannot call EntityHelper::updateFieldValues() on an unsaved entity.'));
    }

    // Some modules use the original property.
    if (!isset($entity->original)) {
      $entity->original = $entity;
    }

    // Ensure that file_field_update() will not trigger additional usage.
    unset($entity->revision);

    // Invoke the field presave and update hooks.
    field_attach_presave($entity_type, $entity);
    field_attach_update($entity_type, $entity);

    // Clear the cache for this entity now.
    entity_get_controller($entity_type)->resetCache(array($id));
  }

  /**
   * An lightest-weight version of entity save that invokes field storage.
   *
   * @param string $entity_type
   *   The entity type of $entity.
   * @param object $entity
   *   The entity object to update.
   * @param array $fields
   *   (optional) An optional array of field names that if provided will only
   *   cause those specific fields to be saved, if values are provided.
   * @throws InvalidArgumentException
   */
  public static function updateFieldValuesStorage($entity_type, $entity, array $fields = array()) {
    list($id, , $bundle) = entity_extract_ids($entity_type, $entity);
    if (empty($id)) {
      throw new InvalidArgumentException(t('Cannot call EntityHelper::updateFieldValues() on an unsaved entity.'));
    }

    // Collect the storage backends used by the remaining fields in the entities.
    $storages = array();
    foreach (field_info_instances($entity_type, $bundle) as $instance) {
      $field = field_info_field_by_id($instance['field_id']);
      $field_id = $field['id'];
      $field_name = $field['field_name'];

      // Check if we care about saving this field or not.
      if (!empty($fields) && !in_array($field_name, $fields)) {
        continue;
      }

      // Leave the field untouched if $entity comes with no $field_name property,
      // but empty the field if it comes as a NULL value or an empty array.
      // Function property_exists() is slower, so we catch the more frequent
      // cases where it's an empty array with the faster isset().
      if (isset($entity->$field_name) || property_exists($entity, $field_name)) {
        // Collect the storage backend if the field has not been written yet.
        if (!isset($skip_fields[$field_id])) {
          $storages[$field['storage']['type']][$field_id] = $field_id;
        }
      }
    }

    // Field storage backends save any remaining unsaved fields.
    foreach ($storages as $storage => $storage_fields) {
      $storage_info = field_info_storage_types($storage);
      module_invoke($storage_info['module'], 'field_storage_write', $entity_type, $entity, FIELD_STORAGE_UPDATE, $storage_fields);
    }

    // Clear the cache for this entity now.
    entity_get_controller($entity_type)->resetCache(array($id));
  }

  /**
   * Return an array of all the revision IDs of a given entity.
   *
   * @param string $entity_type
   *   The entity type of $entity.
   * @param object $entity
   *   The entity object.
   * @param boolean $exclude_current_revision_id
   *   (optional) If TRUE will exclude the current revision of $entity from the
   *   results.
   *
   * @return array
   *   An array of revision IDs associated with $entity.
   */
  public static function getAllRevisionIDs($entity_type, $entity, $exclude_current_revision_id = FALSE) {
    $info = entity_get_info($entity_type);

    if (empty($info['entity keys']['id']) || empty($info['entity keys']['revision']) || empty($info['revision table'])) {
      return array();
    }

    list($entity_id, $revision_id) = entity_extract_ids($entity_type, $entity);
    $id_key = $info['entity keys']['id'];
    $revision_key = $info['entity keys']['revision'];

    $query = db_select($info['revision table'], 'revision');
    $query->addField('revision', $revision_key);
    $query->condition('revision.' . $id_key, $entity_id);
    if ($exclude_current_revision_id) {
      $query->condition('revision.' . $revision_key, $revision_id, '<>');
    }
    return $query->execute()->fetchCol();
  }

  /**
   * Return a render API array of all operations associated with an entity.
   *
   * This depends on modules providing operations as contextual links
   * under the 'base path' of the entity.
   *
   * @param string $entity_type
   *   The entity type of $entity.
   * @param object $entity
   *   The entity object.
   * @param string $base_path
   *   (optional) If the base path of this entity type cannot be automatically
   *   derived from entity_uri(), then a manual override can be provided.
   * @return array
   */
  public static function getOperationLinks($entity_type, $entity, $base_path = NULL) {
    $build = array(
      '#theme' => 'links__operations__' . $entity_type,
      '#links' => array(),
      '#attributes' => array('class' => array('links inline')),
    );

    list($entity_id) = entity_extract_ids($entity_type, $entity);

    // Attempt to extract the base path from the entity URI.
    if (!isset($base_path)) {
      $uri = entity_uri($entity_type, $entity);
      if (empty($uri['path'])) {
        return array();
      }
      $base_path = preg_replace('/\/' . preg_quote($entity_id) . '\b.*/', '', $uri['path']);
    }

    $items = menu_contextual_links($entity_type, $base_path, array($entity_id));
    $links = array();
    foreach ($items as $class => $item) {
      $class = drupal_html_class($class);
      $links[$class] = array(
        'title' => $item['title'],
        'href' => $item['href'],
      );
      $item['localized_options'] += array('query' => array());
      $item['localized_options']['query'] += drupal_get_destination();
      $links[$class] += $item['localized_options'];
    }
    $build['#links'] = $links;

    drupal_alter('contextual_links_view', $build, $items);

    if (empty($links)) {
      $build['#printed'] = TRUE;
    }

    return $build;
  }

  public static function getBundleLabel($entity_type, $bundle) {
    $info = entity_get_info($entity_type);
    return !empty($info['bundles'][$bundle]['label']) ? $info['bundles'][$bundle]['label'] : FALSE;
  }


  public static function getValues($entity_type, $entity, $field_name, $column = NULL) {
    if (!empty($entity) && $items = field_get_items($entity_type, $entity, $field_name)) {
      if (isset($column)) {
        $items = ZMArray::extractNestedValuesToArray($items, array($column));
      }

      return $items;
    }

    return FALSE;
  }

  public static function getValue($entity_type, $entity, $field_name, $column = NULL, $delta = 0) {
    $items = static::getValues($entity_type, $entity, $field_name, $column);
    if (isset($items[$delta])) {
      return $items[$delta];
    }

    return FALSE;
  }


  public static function extractValues($entity_type, $entity, $field, $column) {
    if (!empty($entity) && $items = field_get_items($entity_type, $entity, $field)) {
      return ZMArray::extractNestedValuesToArray($items, array($column));
    }
    return array();
  }

  public static function getValueDelta($entity_type, $entity, $field, $column, $value) {
    if (!empty($entity) && $items = field_get_items($entity_type, $entity, $field)) {
      foreach ($items as $delta => $item) {
        if (isset($item[$column]) && $item[$column] == $value) {
          return $delta;
        }
      }
    }

    return FALSE;
  }

  public static function hasDelta($entity_type, $entity, $field, $delta) {
    if (!empty($entity) && $items = field_get_items($entity_type, $entity, $field)) {
      return !empty($items[$delta]);
    }
  }

  public static function viewValue($entity_type, $entity, $field_name, $delta = 0, $display = array(), $langcode = NULL) {
    $output = array();

    if ($item = static::getValue($entity_type, $entity, $field_name, NULL, $delta)) {
      // Determine the langcode that will be used by language fallback.
      $langcode = field_language($entity_type, $entity, $field_name, $langcode);

      // Push the item as the single value for the field, and defer to
      // field_view_field() to build the render array for the whole field.
      $clone = clone $entity;
      $clone->{$field_name}[$langcode] = array($item);
      $elements = field_view_field($entity_type, $clone, $field_name, $display, $langcode);

      // Extract the part of the render array we need.
      $output = isset($elements[0]) ? $elements[0] : array();
      if (isset($elements['#access'])) {
        $output['#access'] = $elements['#access'];
      }
    }

    return $output;
  }

  /**
   * Return an array of fields with a certain type.
   *
   * @param string $type
   *   The type of field to look for.
   *
   * @return array
   *   An array of fields, keyed by field name.
   */
  public static function getFieldsByType($type) {
    $fields = function_exists('field_info_field_map') ? field_info_field_map() : field_info_fields();
    return array_filter($fields, function ($field) use ($type) {
      return $field['type'] == $type;
    });
  }

  public static function getReferencingFields($field_name = NULL) {
    $results = array();

    if ($cache = cache_get('helper:referencing-field-info', 'cache_field')) {
      $results = $cache->data;
    }
    else {
      $entity_info = entity_get_info();
      $base_tables = array();
      foreach ($entity_info as $type => $type_info) {
        if (!empty($type_info['base table']) && !empty($type_info['entity keys']['id'])) {
          $base_tables[$type_info['base table']] = array('type' => $type, 'column' => $type_info['entity keys']['id']);
        }
      }

      $fields = field_info_fields();
      foreach ($fields as $field_name => $field) {
        // Cannot rely on entityreference fields having correct foreign key info.
        // @todo Remove when http://drupal.org/node/1969018 is fixed.
        if ($field['type'] != 'entityreference') {
          foreach ($field['foreign keys'] as $foreign_key) {
            if (isset($base_tables[$foreign_key['table']])) {
              $base_table = $base_tables[$foreign_key['table']];
              if ($column = array_search($base_table['column'], $foreign_key['columns'])) {
                $results[$field_name] = $base_table;
              }
            }
          }
        }
        else {
          // Special handling for entity reference fields.
          $type = $field['settings']['target_type'];
          if (!empty($entity_info[$type]['base table']) && !empty($entity_info[$type]['entity keys']['id'])) {
            $results[$field_name] = array('type' => $type, 'column' => 'target_id');
          }
        }
      }

      cache_set('helper:referencing-field-info', $results, 'cache_field');
    }

    if (isset($field_name)) {
      return !empty($results[$field_name]) ? $results[$field_name] : FALSE;
    }
    else {
      return $results;
    }
  }

  public static function NodeFieldQuery($node_type, $status = 1){
    $query = new ZMEntityQuery();
    $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $node_type)
      ->propertyCondition('status', $status)
      ->addExtraField('','title','title','node')
    ;

    return $query;
  }

  public static function CreateNewNode($type, $options = array()){
    $default = array(
      'type' => $type,
    );
    $options = $options + $default;
    $entityType = 'node';
    $e  = entity_create($entityType, $options);
    $ew = entity_metadata_wrapper($entityType,$e);
    return $ew;
  }
}

class ZMEntityFieldOr{
  public $fields = array();
  public $fieldConditions = array();

  public function fieldCondition($field, $column = NULL, $value = NULL, $operator = NULL, $delta_group = NULL, $language_group = NULL){
    return $this->addFieldCondition($this->fieldConditions, $field, $column, $value, $operator, $delta_group, $language_group);
  }

  protected function addFieldCondition(&$conditions, $field, $column = NULL, $value = NULL, $operator = NULL, $delta_group = NULL, $language_group = NULL) {
    // The '!=' operator is deprecated in favour of the '<>' operator since the
    // latter is ANSI SQL compatible.
    if ($operator == '!=') {
      $operator = '<>';
    }
    if (is_scalar($field)) {
      $field_definition = field_info_field($field);
      if (empty($field_definition)) {
        throw new EntityFieldQueryException(t('Unknown field: @field_name', array('@field_name' => $field)));
      }
      $field = $field_definition;
    }
    // Ensure the same index is used for field conditions as for fields.
    $index = count($this->fields);
    $this->fields[$index] = $field;
    if (isset($column)) {
      $conditions[$index] = array(
        'field' => $field,
        'column' => $column,
        'value' => $value,
        'operator' => $operator,
        'delta_group' => $delta_group,
        'language_group' => $language_group,
      );
    }
    return $this;
  }
}

/**
 * {@inheritDoc}
 */
class ZMEntityQuery extends EntityFieldQuery{

  private $propertiesOr = array();
  private $preExeIterator;
  private $addedFields = array();
  /**
   * {@inheritDoc}
   * @return ZMEntityQuery
   */
  public function entityCondition($name, $value, $operator = NULL) {
    return parent::entityCondition($name, $value, $operator);
  }
  /**
   * {@inheritDoc}
   * @return ZMEntityQuery
   */
  public function fieldCondition($field, $column = NULL, $value = NULL, $operator = NULL, $delta_group = NULL, $language_group = NULL) {
    return parent::fieldCondition($field, $column, $value, $operator, $delta_group, $language_group);
  }
  /**
   * {@inheritDoc}
   * @return ZMEntityQuery
   */
  public function propertyCondition($column, $value, $operator = NULL) {
    return parent::propertyCondition($column, $value, $operator);
  }
  /**
   * {@inheritDoc}
   * @return ZMEntityQuery
   */
  public function entityOrderBy($name, $direction = 'ASC') {
    return parent::entityOrderBy($name, $direction);
  }
  /**
   * {@inheritDoc}
   * @return ZMEntityQuery
   */
  public function fieldOrderBy($field, $column, $direction = 'ASC') {
    return parent::fieldOrderBy($field, $column, $direction);
  }
  /**
   * {@inheritDoc}
   * @return ZMEntityQuery
   */
  public function propertyOrderBy($column, $direction = 'ASC') {
    return parent::propertyOrderBy($column, $direction);
  }

  /**
   * Add property or condition with db_or.
   *
   * @param $or
   * @return ZMEntityQuery
   */
  public function propertyConditionOr($or) {
    $this->propertiesOr[] = $or;
    return $this;
  }

  /**
   * Finishes the query.
   *
   * Adds tags, metaData, range and returns the requested list or count.
   *
   * @param SelectQuery $select_query
   *   A SelectQuery which has entity_type, entity_id, revision_id and bundle
   *   fields added.
   * @param string $id_key
   *   Which field's values to use as the returned array keys.
   *
   * @return array See EntityFieldQuery::execute().
   */
  function finishQuery($select_query, $id_key = 'entity_id') {
    // http://drupal.org/node/1226622#comment-6809826 - adds support for IS NULL
    // Iterate through all fields.  If the query is trying to fetch results
    // where a field is null, then alter the query to use a LEFT OUTER join.
    // Otherwise the query will always return 0 results.

    // Add property OR condition.
    foreach ($this->propertiesOr as $or) {
      $select_query->condition($or);
    }

    $tables =& $select_query->getTables();
    foreach ($this->fieldConditions as $key => $fieldCondition) {
      if ($fieldCondition['operator'] == 'IS NULL' && isset($this->fields[$key]['storage']['details']['sql'][FIELD_LOAD_CURRENT])) {
        $keys = array_keys($this->fields[$key]['storage']['details']['sql'][FIELD_LOAD_CURRENT]);
        $sql_table = reset($keys);
        foreach ($tables as $table_id => $table) {
          if ($table['table'] == $sql_table) {
            $tables[$table_id]['join type'] = 'LEFT OUTER';
          }
        }
      }
    }

    foreach ($this->tags as $tag) {
      $select_query->addTag($tag);
    }
    foreach ($this->metaData as $key => $object) {
      $select_query->addMetaData($key, $object);
    }
    $select_query->addMetaData('entity_field_query', $this);
    if ($this->range) {
      $select_query->range($this->range['start'], $this->range['length']);
    }
    if ($this->count) {
      if($this->preExeIterator){
        call_user_func($this->preExeIterator, $select_query);
      }
      return $select_query->countQuery()->execute()->fetchField();
    }
    $return = array();

    foreach($this->addedFields as $addedField) {
      $fields = $select_query->getFields();
      if (!empty($addedField['field_name'])) {
        $tables = $select_query->getTables();
        $clean_tables = $this->cleanTables($tables);
        // hardcoded as it is also hardcoded in the fields module
        $table = 'field_data_' . $addedField['field_name'];
        // Get our alias for the selected field
        if (isset($clean_tables[$table])) {
          $addedField['table'] = $clean_tables[$table]['alias'];
        }

        // Set our name and alias
        $column = $addedField['field_name'] . '_' . $addedField['column'];
        $column_alias = /*$addedField['field_name'] . '_' . */$addedField['column_alias'];
      }
      else {
        // Not from a field, so probably a direct entity property
        $column = $addedField['column'];
        $column_alias = $addedField['column_alias'];
      }
      if (!empty($addedField['table'])) {
        // if we know the exact table, set it
        $select_query->addField($addedField['table'], $column, $column_alias);
      }
      else {
        // If not, use the main selected table to fetch the extra field from
        $select_query->addField($fields['entity_id']['table'], $column, $column_alias);
      }
    }

    $tables = $select_query->getTables();
    $clean_tables = $this->cleanTables($tables);

    foreach($this->fieldsConditionOr as $conditionOr){
      $or = db_or();
      foreach ($conditionOr->fieldConditions as $condition) {
        $table = 'field_data_' . $condition['field']['field_name'];
        if($table_alias = $this->getTableAlias($table, $clean_tables)){
          $field_query = $table_alias . "." . $condition['field']['field_name'] . "_" . $condition["column"];
          $or->condition($field_query, $condition['value'], $condition['operator']);
        }
      }
      if (count($or->conditions()) > 0) {
        $select_query->condition($or);
      }
    }

    if($this->preExeIterator){
      call_user_func($this->preExeIterator, $select_query);
    }

    foreach ($select_query->execute() as $partial_entity) {
      $bundle = isset($partial_entity->bundle) ? $partial_entity->bundle : NULL;
      $entity = entity_create_stub_entity($partial_entity->entity_type, array($partial_entity->entity_id, $partial_entity->revision_id, $bundle));
      // This is adding the file id using our metaData field.
      $entity->extraFields = $partial_entity;

      // if the id already exists, merge the data in a smart way. This
      // is completely based on the assumption that we expect a similar entity
      if (isset($return[$partial_entity->entity_type][$partial_entity->$id_key])) {
        $previous_entity = $return[$partial_entity->entity_type][$partial_entity->$id_key];
        foreach ($previous_entity->extraFields as $id => $child) {
          // if the key is the same but the value is not, make it into an array
          if ($entity->extraFields->{$id} != $previous_entity->extraFields->{$id}) {
            $result = array($entity->extraFields->{$id}, $previous_entity->extraFields->{$id});
            $entity->extraFields->{$id} = $result;
          }
        }
      }
      foreach (get_object_vars($entity->extraFields) as $key => $val) {
        $entity->{$key} = $val;
      }
      unset($entity->extraFields);
      // Add the entitiy to the result set to return
      $return[$partial_entity->entity_type][$partial_entity->$id_key] = $entity;
      $this->ordered_results[] = $partial_entity;
    }
    return $return;
  }

  private function getTableAlias($table, $tables = array()){
    static $addedTables = array();
    if(isset($addedTables[$table])){
      return $addedTables[$table];
    }
    foreach ($tables as $alias => $item) {
      if($item['table'] == $table){
        $addedTables[$table] = $item['alias'];
        return $addedTables[$table];
      }
    }

    return FALSE;
  }

  /**
   * @param $field_name
   * @param $column
   * @param string $column_alias
   * @param string $table
   * @return $this
   */
  public function addExtraField($field_name, $column, $column_alias = NULL, $table = NULL) {
    if (!empty($field_name) && !$this->checkFieldExists($field_name)) {
      // Add the field as a condition, so we generate the join
      $this->fieldCondition($field_name);
    }

    $this->addedFields[] = array(
      'field_name' => $field_name,
      'column' => $column,
      'column_alias' => $column_alias,
      'table' => $table,
    );
    return $this;
  }

  /**
   * Give the values in the array the name of the real table instead of the
   * alias, so we can look up the alias quicker
   * @param string $tables
   * @return array
   */
  private function cleanTables($tables) {
    if (!is_array($tables)) {
      return array();
    }
    foreach ($tables as $table_id => $table) {
      if ($table['join type'] == 'INNER') {
        $tables[$table['table']] = $table;
        unset($tables[$table_id]);
      }
    }
    return $tables;
  }
  public function preExecuteQuery($iterator = NULL) {
    $this->preExeIterator = $iterator;
    return $this;
  }

  private $fieldsConditionOr = array();
  public function fieldConditionOr(ZMEntityFieldOr $or) {
    $this->fieldsConditionOr[] = $or;
    foreach($or->fields as $field){
      $field_name = $field['field_name'];
      if (!empty($field_name) && !$this->checkFieldExists($field_name)) {
        $this->fieldCondition($field_name);
      }
    }
    return $this;
  }
  /**
   * Check if the field already has a table that does a join.
   * @param type $field_name
   * @return boolean
   */
  private function checkFieldExists($field_name) {
    $fields = $this->fields;
    foreach($fields as $field) {
      if (isset($field['field_name']) && $field['field_name'] == $field_name) {
        return TRUE;
      }
    }
    return FALSE;
  }
}
