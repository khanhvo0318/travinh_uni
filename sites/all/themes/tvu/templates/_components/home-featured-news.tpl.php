<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 5:30 PM
 */
$query = new ZMEntityQuery();
$query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'article')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_categories', 'tid', 4, '=')
      ->propertyOrderBy('nid', 'DESC')
      ->range(0, 1);
$result = $query->execute();
if (isset($result['node'])) {
  $news_items_nids = array_keys($result['node']);
  $news_items = entity_load('node', $news_items_nids);
  $node = node_view_multiple($news_items, 'featured');
  print render($node);
}
?>


