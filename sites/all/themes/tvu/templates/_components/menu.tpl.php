<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 3:38 PM
 */
$menus = menu_tree_all_data('main-menu');
?>
<div class="header-menu">
  <div class="row">
    <div class="column">
      <ul class="menu">
        <li><a href="/" class="link-home"><img src="<?php print base_path().path_to_theme() ?>/images/icon_home.png" alt=""></a></li>
        <?php if(user_is_anonymous()): ?>
        <li class="link-mb-login"><a href="<?php print url('dang-nhap'); ?>">Đăng Nhập</a></li>
          <?php else: ?>
          <li class="link-mb-login"><a href="<?php print url('user/logout'); ?>">Thoát</a></li>
        <?php endif;?>
        <?php foreach ($menus as $menu):?>
          <?php if(empty($menu['below'])) :?>
            <li><?php print l($menu['link']['link_title'], $menu['link']['link_path']); ?></li>
            <?php else: ?>
            <li>
              <a href="javascript: void(0);" class="item-menu"><?php print $menu['link']['link_title'] ?></a>
              <div class="sub-menu">
                <?php foreach($menu['below'] as $sub): ?>
                  <?php if(empty($sub['below'])) : ?>
                    <?php print l($sub['link']['link_title'], $sub['link']['link_path'], array('attributes' => array('class' => array('sub-item-parent')))); ?>
                    <?php else: ?>
                    <?php print l($sub['link']['link_title'], $sub['link']['link_path'], array('attributes' => array('class' => array('sub-item-parent')))); ?>
                    <?php foreach($sub['below'] as $sub1): ?>
                      <?php print l($sub1['link']['link_title'], $sub1['link']['link_path'], array('class' => array(''))); ?>
                    <?php endforeach;?>
                  <?php endif;?>
                <?php endforeach;?>
              </div>
            </li>
          <?php endif;?>
        <?php endforeach;?>
      </ul>
    </div>
  </div>
</div>
