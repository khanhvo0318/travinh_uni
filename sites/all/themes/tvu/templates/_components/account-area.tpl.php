<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 10:25 AM
 */
global $user;
if(user_is_anonymous()):
?>
<ul>
  <li><a href="<?php print url('node/93') ?>">Đăng Ký</a><span>/</span></li>
  <li><a href="#" class="link-signin">Đăng Nhập</a></li>
</ul>
<div class="dropdown-signin">
  <form name="" method="post" action="<?php print url('quick-login'); ?>" target="quick-login">
  <div class="dropdown-signin-content">
    <input type="text" name="username" placeholder="Email" />
    <input type="password" name="password" placeholder="Mật khẩu" />
    <a href="<?php print url('node/95') ?>">Quên mật khẩu</a>
    <div class="dropdown-submit">
      <div class="custom-checkbox">
        <input type="checkbox" name="checkbox" id="keep-signin"/>
        <label for="keep-signin">Nhớ đăng nhập.</label>
      </div>
      <input type="submit" value="Đăng Nhập" />
    </div>
    <div id="login-message"></div>
  </div>
  </form>
</div>
<?php else:
  $user = user_load($user->uid);
  $notification = _tvu_GetNewNotification($user->uid);
  ?>
  <ul class="header-notification">
    <li><span><?php print $user->field_profile_fullname['und'][0]['value']; ?></span></li>
    <li>
      <?php if(!tvu_checkIsWebAdmin($user->roles, array('administrator', 'webmaster', 'Thư ký'))) : ?>
        <a href="<?php print url('user/'.$user->uid) ?>" class="">Tài khoản</a>
        <?php else: ?>
        <a href="<?php print url('admin/articles-manager') ?>" class="">Admin CP</a>
      <?php endif;?>
      <span class="notification-icon"><i><?php print count($notification) > 0 ? count($notification) : ""  ?></i></span>
      <span> | </span>
      <a href="<?php print url('user/logout') ?>" class="">Thoát</a>
    </li>
  </ul>
  <?php if(!empty($notification)) : ?>
  <div class="dropdown-notification">
    <ul>
      <?php $i=0; foreach ($notification as $item):
        if($i > 3) break;
        ?>
        <li>
          <p><?php print $item->message; ?><span class="date"><?php print date('h:i', $item->created); ?></span></p>
        </li>
      <?php $i++; endforeach;?>
    </ul>
    <div class="view-all"><?php print l('Xem tất cả', 'node/123'); ?></div>
  </div>
  <?php endif;?>
<?php endif;?>
<iframe name="quick-login" id="quick-login-iframe" frameborder="0" scrolling="no"></iframe>
