<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 3:37 PM
 */
?>
<div class="header-top">
  <div class="row">
    <div class="column">
      <a href="/" class="logo">
        <img src="<?php print base_path().path_to_theme() ?>/images/logo.png" alt="" />
        <span class="logo-note">ISSN: 1859-4816</span>
      </a>
      <h2 class="title-website">TẠP CHÍ KHOA HỌC <span>ĐẠI HỌC TRÀ VINH</span></h2>
      <div class="header-search">
        <div class="header-search-main">
          <form action="<?php print url('ket-qua-tim-kiem') ?>" method="get">
            <input type="text" name="keyword" placeholder="Tìm kiếm..." value="<?php print check_plain(Drupal::issetOr($_GET['keyword'], '')); ?>" />
            <div class="advance-form">
              <ul>
                <li>
                  <div class="custom-radio">
                    <input type="radio" <?php print Drupal::issetOr($_GET['search-type'], '') == 'author' ? 'checked' : ""; ?> value="author" name="search-type" id="search-author"/>
                    <label for="search-author">Theo tác giả</label>
                  </div>
                </li>
                <li>
                  <div class="custom-radio">
                    <input type="radio" value="title" <?php print Drupal::issetOr($_GET['search-type'], '') == 'title' ? 'checked' : ""; ?> name="search-type" id="search-title"/>
                    <label for="search-title">Theo tựa đề</label>
                  </div>
                </li>
              </ul>
              <input type="submit" value="Tìm Kiếm" />
            </div>
          </form>
        </div>

        <?php require dirname(__FILE__) . "/../_components/account-area.tpl.php"; ?>
        
      </div>
    </div>
  </div>
</div>
