<header id="header" class="section-header">
  <?php require dirname(__FILE__) . "/../_components/header.tpl.php"; ?>
  <?php require dirname(__FILE__) . "/../_components/menu.tpl.php"; ?>
</header>
<section id="main" class="section-main">
  <?php if ($primary_local_tasks || $secondary_local_tasks || $action_links): ?>
    <div id="tasks">

      <?php if ($primary_local_tasks): ?>
        <ul class="tabs primary clearfix"><?php print render($primary_local_tasks); ?></ul>
      <?php endif; ?>

      <?php if ($secondary_local_tasks): ?>
        <ul class="tabs secondary clearfix"><?php print render($secondary_local_tasks); ?></ul>
      <?php endif; ?>

      <?php if ($action_links = render($action_links)): ?>
        <ul class="action-links clearfix"><?php print $action_links; ?></ul>
      <?php endif; ?>

    </div>
  <?php endif; ?>
  <?php if ($title): ?>
    <h1 id="page-title">
      <?php print $title; ?>
    </h1>
  <?php endif; ?>
  <?php require dirname(__FILE__) . "/../_components/home-featured-news.tpl.php"; ?>
  <div class="row container-main">
    <?php if (isset($page['content'])): ?>
      <div class="column main-left">
      <?php print render($page['content']); ?>
      </div>
    <?php endif; ?>

    <?php if (isset($page['sidebar_first'])): ?>
      <div class="column main-sidebar">
      <?php print render($page['sidebar_first']); ?>
      </div>
    <?php endif; ?>
  </div>
</section>
<footer id="footer" class="section-footer">
  <?php require dirname(__FILE__) . "/../_components/footer.tpl.php"; ?>
</footer>