<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 8:34 PM
 */
//dpm($content);
?>
<div class="cat-item-first">
  <?php $content['field_image']['#theme'] = ''; print render($content['field_image']) ?>
  <h3><a href="<?php print url('node/'.$nid) ?>"><?php print $title; ?></a></h3>
  <p class="desc-item"><?php $content['body']['#theme'] = ''; print strip_tags(render($content['body'])); ?></p>
</div>

