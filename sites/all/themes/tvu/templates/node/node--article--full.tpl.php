<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 2:33 PM
 */
?>
<div class="img-news-detail">
  <?php $content['field_image']['#theme'] = ''; print render($content['field_image']) ?>
</div>
<h5 class="title-news-first"><?php print $title; ?></h5>
<div class="container-news-first">
  <?php $content['body']['#theme'] = ''; print render($content['body']); ?>
</div>
