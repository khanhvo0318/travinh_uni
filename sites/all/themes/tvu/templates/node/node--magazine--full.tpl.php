<?php
$node = node_load($nid);
?>
<div class="page-node-magazine-detail">
  <h5 class="title-news-first"><?php print $title; ?></h5>
  <?php foreach($node->field_file_list['und'] as $list):
    $files_list = field_collection_item_load($list['value']);
    ?>
  <div class="chapter-list">
    <?php foreach ($files_list->field_chapter['und'] as $chapter):?>
    <div class="chapter-item">
      <div class="chapter-title"><?php print $chapter['value']; ?></div>
      <?php foreach($files_list->field_noi_dung['und'] as $file):
          $files_collection = field_collection_item_load($file['value']);
          $pdf = file_create_url($files_collection->field_file_attached['und'][0]['uri']);
      ?>
      <div class="chapter-files">
        <div class="left">
          <div class="title-link">
            <a href="<?php print $pdf; ?>" target="_blank"><?php print $files_collection->field_content_title['und'][0]['value']; ?></a>
          </div>
          <div class="author-name"><?php print $files_collection->field_content_desc['und'][0]['value']; ?></div>
        </div>
        <div class="right"><?php print $files_collection->field_content_page['und'][0]['value']; ?></div>
      </div>
      <?php endforeach; ?>
    </div>
    <?php endforeach; ?>
  </div>
  <?php endforeach; ?>
  <div class="container-news-first">
    <?php $content['body']['#theme'] = ''; print render($content['body']); ?>
  </div>
</div>