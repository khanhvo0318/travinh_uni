<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 5:45 PM
 */
//dpm($content);
?>
<div class="cat-news">
  <div class="row">
    <div class="column">
      <figure>
        <a href="<?php print url($content['field_categories'][0]['#href']); ?>" class="tag-cat tag-cat-mb"><?php print $content['field_categories'][0]['#title'] ?></a>
        <?php $content['field_image']['#theme'] = ''; print render($content['field_image']) ?>
      </figure>
      <div class="content">
        <a href="<?php print url($content['field_categories'][0]['#href']); ?>" class="tag-cat"><?php print $content['field_categories'][0]['#title'] ?></a>
        <h3><a href="<?php print url('node/'.$nid) ?>" class="title"><?php print $title; ?></a></h3>
        <div class="desc"><?php $content['body']['#theme'] = ''; print render($content['body']); ?></div>
        <div class="readmore"><a href="<?php print url('node/'.$nid) ?>">Xem Thêm<i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
      </div>
    </div>
  </div>
</div>
