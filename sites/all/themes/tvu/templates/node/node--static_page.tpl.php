<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 2:27 PM
 */
?>
<h5 class="title-page"><?php print $title; ?></h5>
<div class="main-content">
  <?php print render($content); ?>
</div>
