<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/9/2016
 * Time: 2:49 PM
 */
$authorId = $content['body']['#object']->uid;
$relative = _tvu_getAuthorOtherPost($authorId, $nid);
?>
<div class="section-news section-news-detail">
  <div class="container-news">
    <?php if(!empty($content['field_image'])) :?>
    <div class="img-news-detail">
      <?php $content['field_image']['#theme'] = ''; print render($content['field_image']) ?>
    </div>
    <?php endif; ?>
    <h5 class="title-news-first"><?php print $title; ?></h5>
    <div class="container-news-first">
      <?php $content['body']['#theme'] = ''; print render($content['body']); ?>
    </div>
    <?php if(!empty(arg(2)) && arg(2) == "revisions"): ?>
    <div class="download-link">
      <?php print render($content['field_attached']); ?>
    </div>
    <?php endif; ?>
    <?php if(!empty($relative)):?>
    <div class="news-related">
      <h5 class="head-related">Bài cùng tác giả</h5>
      <div class="news-related-content">
        <?php
        print render($relative['node']); ?>
      </div>
    </div>
      <div class="pagination">
        <?php print theme('pager'); ?>
      </div>
    <?php endif;?>
  </div>
</div>
