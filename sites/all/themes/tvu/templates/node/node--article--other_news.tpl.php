<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 8:37 PM
 */
//Drupal::dpm_one($content);
?>

<div class="item">
  <a href="<?php print url('node/'.$nid); ?>" class="title"><?php print $title; ?></a>
  <span class="time"><?php print format_date($content['body']['#object']->created, 'long') ?></span>
</div>