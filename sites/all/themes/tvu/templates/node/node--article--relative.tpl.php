<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 5/30/2016
 * Time: 8:37 PM
 */
//Drupal::dpm_one($content);
?>

<div class="news-related-item">
  <a href="<?php print url('node/'.$nid); ?>" class="title-related"><?php print $title; ?></a>
  <span class="time-related"><?php print format_date($content['body']['#object']->created, 'long') ?></span>
</div>