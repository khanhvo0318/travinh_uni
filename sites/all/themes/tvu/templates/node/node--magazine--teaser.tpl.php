<?php
$content['field_publish_date']['#theme'] = '';
?>
<div class="page-node-magazine-teaser">
  <h5 class="title-news-first"><?php print l($title, "node/".$nid ); ?>
    <span class="release-date">Phát hành ngày: <?php  print render($content['field_publish_date']) ?></span>
  </h5>
  <div class="container-news-first">
    <?php $content['body']['#theme'] = ''; print render($content['body']); ?>
  </div>
</div>