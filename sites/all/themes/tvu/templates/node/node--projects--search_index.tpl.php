<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/9/2016
 * Time: 2:01 PM
 */
?>
<div class="news-related-item">
  <a href="<?php print url('node/'.$nid); ?>" class="title-related"><?php print $title; ?></a>
  <span class="time-related"><?php print format_date($content['body']['#object']->created, 'long') ?></span>
  <div class="desc"><?php print render($content['body']); ?></div>
</div>
