<?php
/**
 * Created by PhpStorm.
 * User: khanhvo
 * Date: 6/4/2016
 * Time: 9:07 AM
 */
?>
<h5 class="title-news-first"><?php print l($title, 'node/'.$nid, array());?></h5>
<div class="categories-first-image">
  <?php $content['field_image']['#theme'] = ''; print render($content['field_image']) ?>
</div>
<div class="container-news-first">
  <?php print render($content['body']); ?>
</div>
