(function ($) {
    $(document).ready(function () {

        dropdownWebsite();
        slideWebsite()        
        menuMobile();
        renderHtmlMobile();


        var temp = false;
        $(window).resize(function(){
            if(temp != false){
                clearTimeout(temp);
            }
            temp = setTimeout(resizeStuff, 200); //200 is time in miliseconds
        });
        function resizeStuff() {

            
        }        

        function slideWebsite(){
            if($('.slide-magazine').length > 0){
                $('.slide-magazine').owlCarousel({
                    //animateOut: 'fadeOut',
                    loop: true,
                    items: 1,
                    margin: 0,
                    touchDrag: true,
                    mouseDrag: false,
                    dots: false,
                    autoHeight: false,
                    nav: true,
                    dotsSpeed: 500,
                    navText:['',''],
                });
            }
        }
        function dropdownWebsite(){
            if($('.link-signin').length > 0){
                var $linkDropdown = $('.link-signin'),
                    $contentDropdown = $('.dropdown-signin');

                $('html').on('click',function (e){
                    if ($contentDropdown.has(e.target).length === 0 && !$linkDropdown.is(e.target))
                    {
                        $contentDropdown.slideUp('fast');
                    }
                });
                $linkDropdown.on('click',function (e){
                    e.preventDefault();
                    $contentDropdown.slideToggle('fast');
                    console.log('aaaa');
                });
            }
            if($('.header-search-main .advance-form').length > 0){
                $('html').on('click',function (e){
                    if ($('.header-search-main .advance-form').has(e.target).length === 0 && !$('.header-search-main input[type="text"]').is(e.target))
                    {
                        $('.header-search-main .advance-form').slideUp();
                    }
                });
                $('.header-search-main input[type="text"]').focus(function(){
                    $('.header-search-main .advance-form').slideDown();
                });
            }
            if($('.dropdown-notification').length > 0){
                $('html').on('click',function (e){
                    if ($('.dropdown-notification').has(e.target).length === 0 && !$('.notification-icon').is(e.target) && !$('.notification-icon i').is(e.target))
                    {
                        $('.dropdown-notification').slideUp();
                    }
                });
                $('.notification-icon').on('click',function(){
                    $('.dropdown-notification').slideToggle();
                });
            }
        }
        function menuMobile(){
            if($(window).width() < 768){
                if($('.menu-mobile').length > 0) return ;

                // render HTML mobile

                $('.header-menu .column').prepend('<span class="menu-mobile-open"></span>');
                $('.menu-mobile-open, .menu').wrapAll('<div class="menu-mobile" />');

                // render HTML search
                $('.header-menu .column').prepend('<div class="search-mobile"><span class="search-mobile-open"></span><div class="search-mobile-content"></div></div>');
                $('.search-mobile-content').html($('.header-search-main').html());
                $('.header-search-main').remove();

                ////////////////////////////
                $('html').on('click',function (e){
                    if ($('.menu-mobile .menu').has(e.target).length === 0 && !$('.menu-mobile-open').is(e.target))
                    {
                        $('.menu-mobile .menu').slideUp();
                    }
                });
                $('.menu-mobile-open').on('click',function(){
                    $('.menu-mobile .menu').slideToggle();
                });
                $('.item-menu').on('click',function(){
                    var $sub = $(this).next();
                    $sub.slideToggle();
                });
                if($('.search-mobile-content .advance-form').length > 0){
                    $('html').on('click',function (e){
                        if ($('.search-mobile-content .advance-form').has(e.target).length === 0 && !$('.search-mobile-content input[type="text"]').is(e.target))
                        {
                            $('.search-mobile-content .advance-form').slideUp();
                        }
                    });
                    $('.search-mobile-content input[type="text"]').focus(function(){
                        $('.search-mobile-content .advance-form').slideDown();
                    });
                }
            }
        }
        function renderHtmlMobile(){
            if($(window).width() < 768){
                $('.main-left .cat-item').each(function(){
                    var $parent = $(this).find('.cat-item-first'),
                        $readmore = $(this).find('.readmore');
                    $parent.append('<div class="readmore">'+$readmore.html()+'</div>');
                });
                if($('.section-news').length === 0){
                    $('.pagination').remove();
                }                
            }
        }
    });
})(jQuery);