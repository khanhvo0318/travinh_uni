/**
 * Created by khanhvo on 6/4/2016.
 */
(jQuery)(function ($) {
  projectUpdateStatus();
  profileEqualHeighBox();
  removeErrorMessage();
  scrollToError();
  $('#delete-posts').once('sk-delete').click(function (e){
    e.preventDefault();
    checkboxs = $('#table-my-project-list input[type=checkbox]');
    deleted = [];
    checkboxs.each(function (){
      if(this.checked){
        deleted.push(this.value);
      }
    });
    if(deleted.length > 0){
      ask = confirm('Bạn có chắc muốn xóa các bài viết đã chọn');
      if(ask == true){
        $.ajax({
          method: "POST",
          url: Drupal.settings.basePath + "delete-post",
          data: {nids: deleted.join()},
          dataType: "JSON"
        }).done(function (msg) {
          window.location.reload(true);
        });
      }
    }
    return false;
  });
  $("#message-block-header").once('sk-header').on("click", function (e){
    e.preventDefault();
    $(".message-list").slideToggle();
    $.ajax({
      method: "POST",
      url: Drupal.settings.basePath + "process/read-message",
      data: {},
      dataType: "JSON"
    })
  });
  $(".phan-loai-item .block-title").once("sk-click").on("click", function (e){
    e.preventDefault();
    $(this).parent().find('.btt-projects-list').slideToggle();
  });
  $('.table-head .chose-all').once('sk-chose').click(function (e){
    e.preventDefault();
    className = 'is_checked';
    checkboxs = $('#table-my-project-list input[type=checkbox]');
    if($(this).hasClass(className)){
      // unchecked all checkbox
      checkboxs.each(function (){
        $(this).prop('checked', false);
      });
      $(this).removeClass(className);
    } else {
      $(this).addClass(className);
      // check all checkbox
      checkboxs.each(function (){
        $(this).prop('checked', true);
      });
    }
  });
  $('.section-news-detail .container-news-first img').each(function () {
//     $(this).css({'height': '', 'width' : '', 'padding':'0px', 'margin': '0px'});
    $(this).removeAttr('width')
    $(this).removeAttr('height');
    $(this).removeAttr('style');
  });
});
// Drupal.ajax.prototype.commands.showLightBox =  function(ajax, response, status) {
//   showLightbox();
// }

Drupal.behaviors.inviteMoreReview = {
  attach: function (context, settings) {
    inviteMoreReview();
  }
};
function displayErrorMessage(){
  (jQuery)('#login-message').html();
  (jQuery)('#login-message').html('<span>Tên đăng nhập hoặc mật khẩu không chính xác.</span>');
}
function scrollToError(){
  var $ = (jQuery);
  if($(".messages-inline").length > 0){
    topX = $(".messages-inline").first().parents(".section-item").offset().top;
    $('html, body').animate({ scrollTop: topX }, 600);
  }
}
function gotopage(page){
  window.location.href = page;
}

function addNoSideBarClass(){
  (jQuery)(function ($) {
    $("body").addClass('page-no-sidebar');
  });
}

function projectUpdateStatus(){
  (jQuery)(function ($) {
    if($("#project-update-status-select").length > 0){
      $("#project-update-status-select").on("change", function (){
        value = $(this).val();
        if(value != ""){
          $(".invite-review-block").hide();
          if(value == 10){
            $("#btt-hinhthuc").show();
          } else if(value == 11){
            $("#btt-ta").show();
          }  else if(value == -1){
            $("#lydo-khongdang").show();
          } else {
            $("#chon-phan-bien").show();
          }
        }
      });
    }
  });
}

function profileEqualHeighBox(){
  (jQuery)(function($){
    if($('.section-author-account').length > 0){
      // Select and loop the container element of the elements you want to equalise
      $('.section-author-account').each(function(){
        // Cache the highest
        var highestBox = 0;
        // Select and loop the elements you want to equalise
        $('.colum-inbox', this).each(function(){
          // If this box is higher than the cached highest then store it
          if($(this).height() > highestBox) {
            highestBox = $(this).height();
          }
        });
        // Set the height of all those children to whichever was highest
        $('.colum-inbox',this).height(highestBox);
      });
    }
  });
}
function removeErrorMessage(){
  (jQuery)(function ($) {
    if($(".gui-phan-bien-form").length > 0){
      $(".gui-phan-bien-form textarea").on("focus", function (){
        $(this).parents(".section-item").find(".messages-inline").remove();
        $(this).removeClass('error');
      });
      $(".gui-phan-bien-form select").on("change", function (){
        $(this).parents(".section-item").find(".messages-inline").remove();
        $(this).removeClass('error');
      });
    }
  });
}

function showLightbox(title){
  (jQuery)(function ($){
    popup_width = 300;
    $window_width = $( window ).width();
    if($window_width >= 768){ // desktop
      popup_width = 500;
    }
    $("#project-edit-form").dialog({
      resizable: false,
      height: "auto",
      width: popup_width,
      modal: true,
      closeText: "X",
      title: title,
      close: function (event, ui){
        $( "#project-edit-form" ).dialog( "destroy" );
        $("#project-edit-form").html('');
      }
    });
  });
}

function inputNumberOnly(){
  (jQuery)(function ($){
    $(".input-number-only").on('keydown', function(e) {
      var key   = e.keyCode ? e.keyCode : e.which;
      if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
          (key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
          (key >= 96 && key <= 105)
        )) e.preventDefault();
    });
  });
}

function inviteMoreReview(){
  (jQuery)(function ($){
    $("#invite-new-reviews").once("sk-invite").on("click", function (e){
      e.preventDefault();
      $html = $(".invite-group .invite-item:first-child" ).html();
      $close = '<div class="remove-row"><a href="#">Xóa</a></div>';
      $(".invite-group").append('<div class="invite-item">'+$html+$close+'</div>');
      $(".remove-row a").once("process-remove").on("click", function (e){
        e.preventDefault();
        $(this).parents(".invite-item").remove();
        return false;
      });
      return false;
    });
  });

}